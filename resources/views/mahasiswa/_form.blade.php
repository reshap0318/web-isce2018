<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('nim', null, ['class'=>'form-control','maxlength'=>'16', 'minlength'=>'3']) !!}
        <label class="form-label">NIM</label>
    </div>
    <div class="help-info">Min. 3, Max. 16 characters</div>
</div>

<div class="form-group">
        <p>
            <b>Universitas</b>
        </p>
        {!! Form::select('universitas_id', $univ ,null,['class'=>'form-control show-tick', 'required', 'data-live-search'=>'true']) !!}
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('jurusan', null, ['class'=>'form-control']) !!}
        <label class="form-label">Jurusan</label>
    </div>
    <div class="help-info"></div>
</div>

<div class="form-group">
        <label class="form-label">KTM</label>
        {!! Form::file('ktm') !!}
</div>

<div class="form-group">
        <label class="form-label">Bukti Pembayaran</label>
        {!! Form::file('bukti_pembayaran') !!}
</div>
