<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('nama', null, ['class'=>'form-control','maxlength'=>'100', 'minlength'=>'3']) !!}
        <label class="form-label">Nama Kegiatan</label>
    </div>
    <div class="help-info">Min. 3, Max. 100 characters</div>
</div>
<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('tema', null, ['class'=>'form-control','maxlength'=>'50', 'minlength'=>'3']) !!}
        <label class="form-label">Tema Kegiatan</label>
    </div>
    <div class="help-info">Min. 3, Max. 50 characters</div>
</div>
<div class="form-group form-float">
    <div class="form-line">
        {!! Form::date('waktu_mulai', null, ['class'=>'form-control']) !!}
        <label class="form-label">Waktu Mulai</label>
    </div>
</div>
<div class="form-group form-float">
    <div class="form-line">
        {!! Form::date('waktu_berakhir', null, ['class'=>'form-control']) !!}
        <label class="form-label">Waktu Berakhir</label>
    </div>
</div>
<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('insert', null, ['class'=>'form-control']) !!}
        <label class="form-label">Insert Kegiatan</label>
    </div>
    <div class="help-info">gunakan angka (0 - 9)</div>
</div>
<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('lokasi', null, ['class'=>'form-control','maxlength'=>'100', 'minlength'=>'3']) !!}
        <label class="form-label">Lokasi Kegiatan</label>
    </div>
    <div class="help-info">Min. 3, Max. 100 characters</div>
</div>
<div class="form-group form-float">
    <div class="form-line">
        {!! Form::textarea('latar_belakang', null, ['class'=>'form-control']) !!}
        <label class="form-label">Latar Belakang</label>
    </div>
</div>
<div class="form-group form-float">
    <div class="form-line">
        {!! Form::textarea('tujuan', null, ['class'=>'form-control']) !!}
        <label class="form-label">Tujuan</label>
    </div>
</div>
<div class="form-group form-float">
    <div class="form-line">
        {!! Form::textarea('deskripsi', null, ['class'=>'form-control']) !!}
        <label class="form-label">Deskripsi Umum</label>
    </div>
</div>
<div class="form-group">
        {!! Form::file('foto', null, ['class'=>'form-control']) !!}
        <label class="form-label">Foto</label>
</div>
