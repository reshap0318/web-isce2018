<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('nisn', null, ['class'=>'form-control','maxlength'=>'16', 'minlength'=>'3']) !!}
        <label class="form-label">NISN</label>
    </div>
    <div class="help-info">Min. 3, Max. 16 characters</div>
</div> 

<div class="form-group">
    <p>
        <b>Sekolah</b>
    </p>
    {!! Form::select('sekolah_id', $sekolah,null, ['class'=>'form-control show-tick', 'required', 'data-live-search'=>'true']) !!}
</div>

<div class="form-group">
        <label class="form-label">Bukti Pembayaran</label>
        {!! Form::file('bukti_pembayaran') !!}
</div>
