@extends('potongan.main')

@section('title')
	Create Pelajar Profil
@stop

@section('content')
	<div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Create Pelajar Profil</h2>
                </div>
                <div class="body">
                    {{ Form::open(array('url' => route('pelajar-profil.store'), 'files' => true)) }}
                        
                        @include('pelajar_profil._form')

                        <br>
                        <div class="text-center">
                            <button type="reset" class="btn btn-primary m-t-15 waves-effect">Reset</button>
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">Simpan</button>
                            <a href="{{ route('pelajar-profil.index') }}" class="btn btn-primary m-t-15 waves-effect">Back</a>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <!-- Jquery Validation Plugin Css -->
    <script src="{{ asset('template/plugins/jquery-validation/jquery.validate.js') }}"></script>

    <!-- JQuery Steps Plugin Js -->
    <script src="{{ asset('template/plugins/jquery-steps/jquery.steps.js') }}"></script>

    <!-- Sweet Alert Plugin Js -->
    <script src="{{ asset('template/plugins/sweetalert/sweetalert.min.js') }}"></script>

    <script src="{{ asset('template/js/pages/forms/form-validation.js') }}"></script>

    <!-- Multi Select Plugin Js -->
    <script src="{{ asset('template/plugins/multi-select/js/jquery.multi-select.js') }}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ asset('template/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
{{-- 
    <script src="{{ asset('template/js/pages/forms/advanced-form-elements.js') }}"></script>
     --}}
@stop

@section('style')

    <!-- Multi Select Css -->
    <link href="{{ asset('template/plugins/multi-select/css/multi-select.css') }}" rel="stylesheet">

    <!-- Bootstrap Select Css -->
    <link href="{{ asset('template/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />

    <link href="{{ asset('template/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" />
@stop
