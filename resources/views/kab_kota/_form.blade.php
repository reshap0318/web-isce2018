<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('nama_kab_kota', null, ['class'=>'form-control','maxlength'=>'30', 'minlength'=>'3', 'required']) !!}
        <label class="form-label">Nama Kabupaten & Kota</label>
    </div>
    <div class="help-info">Min. 3, Max. 30 characters</div>
</div>

<div class="form-group">
	<p>
	    <b>Provinsi</b>
	</p>
	{!! Form::select('prov_id', $prov,null, ['class'=>'form-control show-tick', 'required', 'data-live-search'=>'true']) !!}
</div>

