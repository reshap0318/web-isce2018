@extends('lomba.index')

@section('kontent')
	<header id="fh5co-header" class="fh5co-cover" role="banner" style="background-image:url({{ asset('awal/images/img_bg_1.jpg') }});" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-7 text-left">
                    <div class="display-t">
                        <div class="display-tc animate-box" data-animate-effect="fadeInUp">
                            <h1 class="mb30">Information System Celebration Event
                               <br> <small>Olimpiade Komputer</small></h1>
                               <p>
                                <a href="{{url('daftar')}}" class="btn btn-primary">Register</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div id="fh5co-project">
		<div class="container">
			<div class="row row-pb-md">
				<div class="col-md-12 text-left fh5co-heading animate-box">
					<span>Panitia ISCE</span>
					<h2>Deskripsi Umum</h2>
					<p style="text-align: justify;">Kegiatan Olimpiade Komputer untuk siswa/i ngkat SMA se-Sumatra Barat yang diadakan dengan dua tahap yakni tahap penyisihan pada 5 November 2018 dan tahap ﬁnal pada 6 November 2018, disamping itu juga diadakan Expo dan Bazar untuk menampilkan bakat dan memeriahkan kegiatan ISCE 2018.</p>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6 col-sm-6 fh5co-project animate-box" data-animate-effect="fadeIn">
					<a href="#"><img src="{{asset('img/olkomtahap1.png')}}" alt="Free HTML5 Website Template by gettemplates.co" class="img-responsive">
						<div class="fh5co-copy">
							<h3>Pendaftaran Tahap I</h3>
							<p>03 september 2018 - 28 september 2018</p>
						</div>
					</a>
				</div>
				<div class="col-md-6 col-sm-6 fh5co-project animate-box" data-animate-effect="fadeIn">
					<a href="#"><img src="{{asset('img/olkomtahap2.png')}}" alt="Free HTML5 Website Template by gettemplates.co" class="img-responsive">
						<div class="fh5co-copy">
							<h3>Pendaftaran Tahap II</h3>
							<p>08 Oktober 2018 - 26 Oktober 2018</p>
						</div>
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-6 fh5co-project animate-box" data-animate-effect="fadeIn">
					<a href="#"><img src="{{asset('img/olkomsemifinal.png')}}" alt="Free HTML5 Website Template by gettemplates.co" class="img-responsive">
						<div class="fh5co-copy">
							<h3>Babak Semifinal</h3>
							<p>05 November 2018</p>
						</div>
					</a>
				</div>
				<div class="col-md-6 col-sm-6 fh5co-project animate-box" data-animate-effect="fadeIn">
					<a href="#"><img src="{{asset('img/olkomfinal.png')}}" alt="Free HTML5 Website Template by gettemplates.co" class="img-responsive">
						<div class="fh5co-copy">
							<h3>Babak Final</h3>
							<p>06 November 2018</p>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
@stop