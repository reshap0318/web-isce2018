@extends('potongan.main')

@section('title')
    Berkas User Olimpiade Komputer
@stop

@section('style')

    <link href="{{ asset('template/plugins/light-gallery/css/lightgallery.css') }}" rel="stylesheet" />

@stop
@section('content')
<!-- Dismissible Alerts -->
    <div class="row clearfix">
        {{-- fotoprofil --}}
        
        @if($pembayaran=="belum")
        {{-- pembayaran --}}
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                Anda Belum Melakukan Pembayaran, Silakan Lakukan Pembayaran dan Kirim Bukti Pembayaran Anda
            </div>
        </div>
        @endif
    </div>
<!-- #END# Dismissible Alerts -->

<!-- Image Gallery -->
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                   Foto Bukti Pembayaran
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">add_a_photo</i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="body text-center text-center">
                <div id="aniimated-thumbnials" class="list-unstyled row clearfix text-center">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                        @if($pembayaran=="sudah")
                            <a href="{{ url('foto/olkom/'.$peserta->user->pelajar->bukti_pembayaran) }}" data-sub-html="Demo Description" class="text-center">
                                <img style="width: 100%;height: 600px"  class="img-responsive thumbnail text-center" src="{{ url('foto/olkom/'.$peserta->user->pelajar->bukti_pembayaran) }}">
                            </a>
                        @endif
                        
                        <form action="{{ url('upload-bayar-olkom/'.Sentinel::getuser()->id)}}" enctype="multipart/form-data" method="POST" style="display:none">
                            @csrf
                            @method('patch')
                            <input type="file" id="olkom-pict" name="olkom" onchange="this.form.submit();"/>
                        </form>
                        
                        <a href="#" onclick="event.preventDefault();openImageUploadDialogolkom();" class="btn btn-success waves-effect">
                            <i class="material-icons">add_a_photo</i> Ganti Foto
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END#Image Gallery -->
@stop

@section('script')
    
    <script src="{{ asset('template/plugins/light-gallery/js/lightgallery-all.js') }}"></script>
    <script type="text/javascript">
        function openImageUploadDialogolkom(){
            $("#olkom-pict").trigger("click");
        }
    </script>
@stop