@extends('potongan.main')

@section('title')
    Daftar Lomba Seminar Nasional
@stop

@section('style')

    <link href="{{ asset('template/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet" />

@stop
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body" style="height: 80px">
                <form>
                    <div class="row clearfix">
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control">
                                    <label class="form-label">Group Name</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <button type="button" class="btn btn-primary btn-lg m-l-20 waves-effect pull-right"  data-toggle="tooltip" data-placement="bottom" title="Nama Grup yang dibuat tidak bisa dirubah kemudian waktu" type="button" style="width: 200px">Make Group</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    <strong>Team Nakniknuk</strong>
                    <small>Cabang Lombo Hackathon</small>
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="btn bg-indigo waves-effect" data-toggle="tooltip" data-placement="bottom" title="Tambahkan Anggota untuk Lomba Hackathon" type="button" data-type="prompt">
                            <i class="material-icons">person_add</i> Tambahkan Anggota
                        </a>
                    </li>
                </ul>
            </div>
            <div class="body table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Nomor Induk Mahasiswa</th>
                            <th>Nomor Handphone</th>
                            <th>Universitas</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>Reinaldo Shandev Pratama</td>
                            <td>1611522012</td>
                            <td>085805506719</td>
                            <td>Universitas Andalas</td>
                            <td>
                                <a href="#" class="btn btn-info btn-xs waves-effect"> Detail</a>
                                <a href="#" class="btn btn-danger btn-xs waves-effect"> Delete</a>
                                {{-- {!! Form::open(['method'=>'DELETE', 'route' => ['sekolah.destroy', $sekolah->id], 'style' => 'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs waves-effect','id'=>'delete-confirm"']) !!}
                                {!! Form::close() !!} --}}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    <strong>Upload Proposal</strong>
                    <small>Silakan upload proposal anda disini, anda hanya di perbolehkan untuk mengupload sebanyak tiga(3) proposal.</small>
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="btn bg-deep-orange waves-effect" data-toggle="tooltip" data-placement="bottom" title="Upload Proposal Hackathon max 3 proposal" type="button" data-type="prompt">
                            <i class="material-icons">file_upload</i> Upload Proposal
                        </a>
                    </li>
                </ul>
            </div>
            <div class="body table-responsive">
                <table class="table">
                    <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>Reinaldo Shandev Pratama</td>
                            <td>29 Agustus 2019</td>
                            <td>
                                <a href="#" class="btn btn-info btn-xs waves-effect"> Detail</a>
                                <a href="#" class="btn btn-danger btn-xs waves-effect"> Delete</a>
                                {{-- {!! Form::open(['method'=>'DELETE', 'route' => ['sekolah.destroy', $sekolah->id], 'style' => 'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs waves-effect','id'=>'delete-confirm"']) !!}
                                {!! Form::close() !!} --}}
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">1</th>
                            <td>Reinaldo Shandev Pratama</td>
                            <td>29 Agustus 2019</td>
                            <td>
                                <a href="#" class="btn btn-info btn-xs waves-effect"> Detail</a>
                                <a href="#" class="btn btn-danger btn-xs waves-effect"> Delete</a>
                                {{-- {!! Form::open(['method'=>'DELETE', 'route' => ['sekolah.destroy', $sekolah->id], 'style' => 'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs waves-effect','id'=>'delete-confirm"']) !!}
                                {!! Form::close() !!} --}}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@stop

@section('script')
    
    <script src="{{ asset('template/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

    <script src="{{ asset('template/js/pages/ui/tooltips-popovers.js') }}"></script>
    <script src="{{ asset('template/js/pages/ui/dialogs.js') }}"></script>

    
@stop