@extends('potongan.main')

@section('title')
    Daftar Lomba Hackathon
@stop

@section('style')
    <link href="{{ asset('template/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet" />
@stop
@section('content')
@if($pes!="v")
{{-- buat grup --}}
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body" style="height: 80px">
                {{ Form::open(array('url' => 'hackathon-gruop', 'files' => true)) }}
                    <div class="row clearfix">
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="nama" class="form-control">
                                    <label class="form-label">Group Name</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <button type="submit" class="btn btn-primary btn-lg m-l-20 waves-effect pull-right"  data-toggle="tooltip" data-placement="bottom" title="Nama Grup yang dibuat tidak bisa dirubah kemudian waktu" style="width: 200px">Make Group</button>
                        </div> 
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endif

@if($pes=="v")
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    <strong>Team Nakniknuk</strong>
                    <small>Cabang Lomba Hackathon</small>
                </h2>
                @if($ketua=="v" && $jumlah_anggota<3)
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown"> 
                        <a href="#collapseExample" class="btn bg-indigo waves-effect" data-placement="bottom" title="Tambahkan Anggota untuk Lomba Hackathon" type="button" data-type="prompt" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false">
                            <i class="material-icons">person_add</i> Tambahkan Anggota
                        </a>
                    </li>
                </ul>
                <div class="collapse" id="collapseExample">
                    <div class="well">
                        {{ Form::open(array('url' => route('hackathon.addanggota'), 'files' => true)) }}
                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                            {!! Form::text('username', null, ['class'=>'form-control','placeholder' => 'Cari Username']) !!}
                        </div>
                            <button style="width: 100px" type="submit" class="btn btn-primary btn-lg m-l-15 waves-effect">Cari</button>
                        {{ Form::close() }}
                    </div>
                </div>
                @endif
            </div>
            {{-- {{dd($peserta->kelompok_id)}} --}}
            {{-- @foreach($peserta as $test)
                {{ var_dump($test)}}
            @endforeach --}}
            <div class="body table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Nomor Induk Mahasiswa</th>
                            <th>Nomor Handphone</th>
                            <th>Universitas</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=0?>
                        @foreach($kelompok as $pp)
                            <tr>
                                <td scope="row">{{++$no}}</td>
                                <td>{{$pp->user->nama}}</td>
                                <td>{{$pp->user->mahasiswa->nim}}</td>
                                <td>{{$pp->user->no_hp}}</td>
                                @if($pp->user->mahasiswa->universitas)
                                    <td>{{$pp->user->mahasiswa->universitas->nama}}</td>
                                @else
                                    <td>Belum ada</td>
                                @endif
                                <td> 
                                    <a href="{{url('profil/'.$pp->user->nama.'/'.$pp->user->id)}}" class="btn btn-info btn-xs waves-effect"> Detail</a>
                                    @if($ketua=="v" && $pp->user_id!=Sentinel::getuser()->id)
                                        {!! Form::open(['method'=>'DELETE', 'route' => ['user-lomba.destroy', $pp->id], 'style' => 'display:inline']) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs waves-effect','id'=>'delete-confirm"']) !!}
                                        {!! Form::close() !!}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    <strong>Upload Proposal</strong>
                    <small>Silakan upload proposal anda disini, anda hanya di perbolehkan untuk mengupload sebanyak tiga(3) proposal.</small>
                </h2>
                @if($jumlah_proposal<3)
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <form action="{{ url('upload-proposal/'.Sentinel::getuser()->id)}}" enctype="multipart/form-data" method="POST" style="display:none">
                            @csrf
                            <input type="file" id="proposal-pict" name="proposal" onchange="this.form.submit();"/>
                        </form>
                        <a href="#" class="btn bg-orange waves-effect" data-toggle="tooltip" data-placement="bottom" title="Tambahkan Proposal untuk Lomba Hackathon" type="button" data-type="prompt" onclick="event.preventDefault();openImageUploadDialoghack();">
                            <i class="material-icons">file_upload</i> Tambahkan Proposal
                        </a>
                    </li>
                </ul>
                @endif
            </div>
            <div class="body table-responsive">
                <table class="table">
                    <tbody>
                        <?php
                            $no = 0;
                        ?>
                        @foreach($proposal as $pro)
                        <tr>
                            <th scope="row">{{++$no}}</th>
                            <td>{{$pro->nama}}</td>
                            <td>{{\Carbon\Carbon::parse($pro->created_at)->format('d F Y')}}</td>
                            <td>
                                <a href="{{ url('foto/proposal/'.$pro->nama) }}" download="{{$pro->nama}}" class="btn btn-info btn-xs waves-effect">
                                    Download
                                </a>
                                
                                {!! Form::open(['method'=>'DELETE', 'route' => ['hackathon.dellproposal', $pro->id], 'style' => 'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs waves-effect','id'=>'delete-confirm"']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endif


@stop

@section('script')
    
    <script src="{{ asset('template/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

    <!-- SweetAlert Plugin Js -->
    <script src="{{ asset('template/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script type="text/javascript">
        function openImageUploadDialoghack(){
            $("#proposal-pict").trigger("click");
        }

        function coba() {
                swal({
                title: "An input!",
                text: "Write something interesting:",
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                animation: "slide-from-top",
                inputPlaceholder: "Write something"
            }, function (inputValue) {
                if (inputValue === false) return false;
                if (inputValue === "") {
                    swal.showInputError("You need to write something!"); return false
                }
                swal("Nice!", "You wrote: " + inputValue, "success");
            });
        }
    </script>
    <script src="{{ asset('template/js/pages/ui/tooltips-popovers.js') }}"></script>
    <script src="{{ asset('template/js/pages/ui/dialogs.js') }}"></script>

    

    
@stop