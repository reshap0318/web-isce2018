@extends('potongan.main')

@section('title')
	Edit Profil {{Sentinel::getuser()->nama}}
@stop

@section('style')

    <!-- Multi Select Css -->
    <link href="{{ asset('template/plugins/multi-select/css/multi-select.css') }}" rel="stylesheet">

    <!-- Bootstrap Select Css -->
    <link href="{{ asset('template/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />

    <link href="{{ asset('template/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" />
@stop
@section('content')

    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body">
                <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                    <div class="text-center">
                        
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row text-center">
                        {{-- <h2>PROFIL</h2> --}}
                        <div class="row">
                            @if(Sentinel::getuser()->foto)
                            <a href="{{url('foto/foto/'.Sentinel::getuser()->foto)}}" data-sub-html="Demo Description">
                                <img style="width: 150px;height: 200px"  class="" src="{{url('foto/foto/'.Sentinel::getuser()->foto)}}">
                            </a>
                            @else
                            <a href="{{url('foto/foto/person.png')}}" data-sub-html="Demo Description">
                                <img style="width: 150px;height: 200px"  class="" src="{{url('foto/foto/'.Sentinel::getuser()->foto)}}">
                            </a>
                            @endif
                            <br>

                            <form action="{{ url('upload-foto/'.Sentinel::getuser()->id)}}" enctype="multipart/form-data" method="POST" style="display:none">
                                @csrf
                                @method('patch')
                                <input type="file" id="foto-pict" name="foto" onchange="this.form.submit();"/>
                            </form>

                            <a href="#" onclick="event.preventDefault();openImageUploadDialogfoto();" class="btn btn-success waves-effect">
                                <i class="material-icons">add_a_photo</i> Ganti Profil
                            </a>
                        </div>

                        <div class="row">
                               <strong>Username</strong> : {{Sentinel::getuser()->username}}
                        </div>
                    </div>

                    {{ Form::open(array('url' => 'perbaruiprofil', 'files' => true, 'class'=>'form-horizontal')) }}
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="nama">Nama Lengkap</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="nama" value="{{Sentinel::getuser()->nama}}" name="nama" class="form-control" placeholder="Nama Lengkap">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                @if(Sentinel::getuser()->mahasiswa)
                                <label for="nim">NIM</label>
                                @else
                                <label for="nisn">NISN</label>
                                @endif
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        @if(Sentinel::getuser()->mahasiswa)
                                        <input name="nim" type="text" id="nim" value="{{Sentinel::getuser()->mahasiswa->nim}}" class="form-control" placeholder="Nomor Induk Mahasiswa">
                                        @else
                                        <input name="nisn" type="text" id="nisn" value="{{Sentinel::getuser()->pelajar->nisn}}" class="form-control" placeholder="Nomor Induk Mahasiswa">
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="email">Email</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input name="email" type="text" id="email" value="{{Sentinel::getuser()->email}}" class="form-control" placeholder="Email">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="no_hp">No HP</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input name="no_hp" type="text" id="no_hp" value="{{Sentinel::getuser()->no_hp}}" class="form-control" placeholder="Nomor HP">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">

                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                @if(Sentinel::getuser()->mahasiswa)
                                <label for="universitas">Universitas</label>
                                @else
                                <label for="sekolah">sekolah</label>
                                @endif
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-7 col-xs-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        @if(Sentinel::getuser()->mahasiswa)
                                            <select name="universitas" class="form-control show-tick" data-live-search="true" select>
                                                <option>--pilihan--</option>
                                                @foreach($universitas as $univ)
                                                    @if($univ->id == Sentinel::getuser()->mahasiswa->universitas_id)
                                                    <option selected="selected" value="{{$univ->id}}">{{$univ->nama}}</option>
                                                    @else
                                                    <option value="{{$univ->id}}">{{$univ->nama}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        @else
                                            <select name="sekolah" class="form-control show-tick" data-live-search="true">
                                                <option>--pilihan--</option>
                                                @foreach($sekolah as $seko)
                                                    @if($seko->id == Sentinel::getuser()->pelajar->sekolah_id)
                                                        <option selected="selected" value="{{$seko->id}}">{{$seko->nama}}</option>
                                                    @else
                                                        <option value="{{$seko->id}}">{{$seko->nama}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @if(Sentinel::getuser()->mahasiswa)
                                <a href="{{url('tambah-universitas')}}" type="button" class="btn bg-deep-orange btn-circle waves-effect waves-circle waves-float">
                                <i class="material-icons">add_circle</i>
                                </a>
                            @else
                                <a type="button" href="{{url('tambah-sekolah')}}" class="btn bg-deep-orange btn-circle waves-effect waves-circle waves-float">
                                <i class="material-icons">add_circle</i>
                                </a>
                            @endif
                        </div>
                        @if(Sentinel::getuser()->mahasiswa)
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">

                                <label for="jurusan">Jurusan</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input name="jurusan" type="text" id="jurusan" value="{{Sentinel::getuser()->mahasiswa->jurusan}}" class="form-control" placeholder="Jurusan">
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="jenis_kelamin">Jenis Kelamin</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select name="jenis_kelamin" class="form-control show-tick">
                                            <option>--pilihan--</option>
                                            @if(Sentinel::getuser()->jenis_kelamin == 'Laki-Laki' || Sentinel::getuser()->jenis_kelamin == 'L')
                                                <option selected="selected" value="Laki-Laki">Laki-Laki</option>
                                                <option value="Perempuan">Perempuan</option>
                                            @else
                                                <option value="Laki-Laki">Laki-Laki</option>
                                                <option selected="selected" value="Perempuan">Perempuan</option>
                                            @endif
                                        </select>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="tempat_lahir">Tempat Lahir</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input name="tempat_lahir" type="text" value="{{Sentinel::getuser()->tempat_lahir}}" id="tempat_lahir" class="form-control" placeholder="Tempat Lahir">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="tanggal_lahir">Tanggal Lahir</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input name="tanggal_lahir" type="text" id="tanggal_lahir" value="{{Sentinel::getuser()->tanggal_lahir}}" class="form-control" placeholder="Tanggal Lahir">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="alamat">Alamat</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input name="alamat" type="text" id="alamat" value="{{Sentinel::getuser()->alamat}}" class="form-control" placeholder="Alamat">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row text-center">
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">Simpan Perubahan</button>
                            <a href="{{ route('home.dashboard') }}" class="btn btn-primary m-t-15 waves-effect">Back</a>
                        </div>
                    {{ Form::close() }}

                    

                    
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('script')
    <!-- Jquery Validation Plugin Css -->
    <script src="{{ asset('template/plugins/jquery-validation/jquery.validate.js') }}"></script>

    <!-- JQuery Steps Plugin Js -->
    <script src="{{ asset('template/plugins/jquery-steps/jquery.steps.js') }}"></script>

    <!-- Sweet Alert Plugin Js -->
    <script src="{{ asset('template/plugins/sweetalert/sweetalert.min.js') }}"></script>

    <script src="{{ asset('template/js/pages/forms/form-validation.js') }}"></script>

    <!-- Multi Select Plugin Js -->
    <script src="{{ asset('template/plugins/multi-select/js/jquery.multi-select.js') }}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ asset('template/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
{{-- 
    <script src="{{ asset('template/js/pages/forms/advanced-form-elements.js') }}"></script>
     --}}
     <script type="text/javascript">

        function openImageUploadDialogfoto(){
            $("#foto-pict").trigger("click");
        }
    </script>
@stop