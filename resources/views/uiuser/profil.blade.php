@extends('potongan.main')

@section('title')
	Profil {{$user->nama}}
@stop

@section('style')

	<link href="{{ asset('template/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet" />

@stop
@section('content')

    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body">
                <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                    <div class="text-center">
                        
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row text-center">
                        {{-- <h2>PROFIL</h2> --}}
                        <div class="row">
                            @if($user->foto)
                            <a href="{{url('foto/foto/'.$user->foto)}}" data-sub-html="Demo Description">
                                <img style="width: 150px;height: 200px"  class="" src="{{url('foto/foto/'.$user->foto)}}">
                            </a>
                            @else
                            <a href="{{url('foto/foto/person.png')}}" data-sub-html="Demo Description">
                                <img style="width: 150px;height: 200px"  class="" src="{{url('foto/foto/'.$user->foto)}}">
                            </a>
                            @endif
                        </div>

                        <div class="row">
                               <strong>Username</strong> : {{$user->username}}
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
                        <div class="form-group row">
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                <strong>Nama</strong>
                            </div>
                            <div>
                                : {{$user->nama}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                @if($user->mahasiswa)
                                    <strong>NIM</strong>
                                @elseif($user->pelajar)
                                    <strong>NISN</strong>
                                @else
                                        <font style="color: red">Anda Belum Terdaftar</font>
                                @endif
                            </div>
                            <div>
                                : 
                                @if($user->mahasiswa)
                                    @if($user->mahasiswa->nim)
                                        {{$user->mahasiswa->nim}}
                                    @else
                                        <font style="color: red">Belum Memasukan NIM</font>
                                    @endif
                                @elseif($user->pelajar)
                                    @if($user->pelajar->nisn)
                                        {{$user->pelajar->nisn}}
                                    @else
                                        <font style="color: red">Belum Memasukan NISN</font>
                                    @endif
                                    
                                @else
                                    <font style="color: red">Belum Terdaftar</font>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                <strong>No Hp </strong>
                            </div>
                            <div>
                                : {{$user->no_hp}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                <strong>Email</strong> 
                            </div>
                            <div>
                                : {{$user->email}}
                            </div>
                        </div>

                        @if($user->mahasiswa)

                            <div class="form-group row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                    <strong>Universitas</strong>
                                </div>
                                <div>
                                    : 
                                    @if($user->mahasiswa->universitas)
                                        {{$user->mahasiswa->universitas->nama}}
                                    @else
                                        <font style="color: red">Belum Memiliki Univeristas</font>
                                    @endif
                                    
                                </div>
                            </div>

                        @elseif($user->pelajar)

                            <div class="form-group row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                    <strong>Asal Sekolah</strong> 
                                </div>
                                <div>
                                    @if($user->pelajar->sekolah)
                                        : {{$user->pelajar->sekolah->nama}}
                                    @else
                                        : <font style="color: red">Belum Mendaftarkan Nama Sekolah</font>
                                    @endif


                                </div>
                            </div>

                        @endif
                        
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-xs-offset-2">
                        
                        @if($user->peserta)
                            @if($user->mahasiswa)
                                    
                                    <div class="form-group row">
                                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                            <strong>Group</strong>
                                        </div>
                                        <div>
                                            @if($user->peserta->kelompok)
                                                : {{$user->peserta->kelompok->nama}}
                                            @else
                                                <font style="color: red">Anda Belum Memiliki Kelompok</font>
                                            @endif
                                        </div>
                                    </div>
                            @elseif($user->pelajar)

                            @else
                                <font style="color: red">Anda Belum Terdaftar</font> 
                            @endif
                        @endif

                        <div class="form-group row">
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                <strong>Jenis Lomba</strong>  
                            </div>
                            <div>
                                : 
                                @if($user->peserta)
                                    {{$user->peserta->kegiatan->nama}}
                                @else
                                    <font style="color: red">Belum Memiliki Cabang Lomba</font>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                <strong>TTL</strong> 
                            </div>
                            <div>
                                : {{$user->tempat_lahir}}, {{$user->tanggal_lahir}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                <strong>Jenis Kelamin</strong> 
                            </div>
                            <div>
                                : 
                                @if($user->jenis_kelamin == 'L' || $user->jenis_kelamin == "Laki-Laki")
                                    Laki - Laki
                                @elseif($user->jenis_kelamin == 'P' || $user->jenis_kelamin == "Perempuan")
                                    Perempuan
                                @else
                                    <font style="color: red">Kelamin Tidak Jelas</font>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                <strong>Alamat</strong>  
                            </div>
                            <div>
                                : {{$user->alamat}}
                            </div>
                        </div>
                        
                        @if($user->mahasiswa)

                            <div class="form-group row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                    <strong>Jurusan</strong>
                                </div>
                                <div>
                                    : 
                                    @if($user->mahasiswa->jurusan)
                                        {{$user->mahasiswa->jurusan}}
                                    @else
                                        <font style="color: red">Belum Memiliki Jurusan</font>
                                    @endif
                                </div>
                            </div>

                        @elseif($user->pelajar)

                        @endif
                    </div>

                @if($user->id == Sentinel::getuser()->id)

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row text-center">
                            <a class="btn btn-primary m-t-15 waves-effect" href="{{url('editprofil/'.$user->nama.'/'.$user->id)}}">Perbarui Profil</a>
                            <a href="{{ route('home.dashboard') }}" class="btn btn-primary m-t-15 waves-effect">Back</a>
                    </div>

                @else
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row text-center">
                            <a href="{{ url('hackathon/'.Sentinel::getuser()->nama.'/'.Sentinel::getuser()->id) }}" class="btn btn-primary m-t-15 waves-effect">Back</a>
                    </div>
                @endif

                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('script')
	
    <script src="{{ asset('template/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

    
@stop