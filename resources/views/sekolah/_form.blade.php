<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('nama', null, ['class'=>'form-control','maxlength'=>'60', 'minlength'=>'3']) !!}
        <label class="form-label">Nama Sekolah *</label>
    </div>
    <div class="help-info">Min. 3, Max. 60 characters</div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('kontak', null, ['class'=>'form-control','maxlength'=>'12', 'minlength'=>'3']) !!}
        <label class="form-label">Kontak Sekolah</label>
    </div>
    <div class="help-info">No Telphon (0 - 9)</div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('nama_kontak', null, ['class'=>'form-control','maxlength'=>'30', 'minlength'=>'3']) !!}
        <label class="form-label">Nama Kontak</label>
    </div>
    <div class="help-info">Min. 3, Max. 30 characters</div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('kode_pos', null, ['class'=>'form-control','maxlength'=>'6', 'minlength'=>'3']) !!}
        <label class="form-label">Kode Pos Sekolah</label>
    </div>
    <div class="help-info">Min. 3, Max. 6 characters</div>
</div>
{{-- 
<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('provinsi', null, ['class'=>'form-control']) !!}
        <label class="form-label">Provinsi</label>
    </div>
    <div class="help-info"></div>
</div>
<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('kab', null, ['class'=>'form-control']) !!}
        <label class="form-label">Kab & Kota</label>
    </div>
    <div class="help-info"></div>
</div>
<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('kecamatan_id', null, ['class'=>'form-control']) !!}
        <label class="form-label">Kecamatan</label>
    </div>
    <div class="help-info"></div>
</div> --}}
<div class="form-group form-float">
    <div class="form-line">
        {!! Form::textarea('alamat', null, ['class'=>'form-control']) !!}
        <label class="form-label">Alamat Sekolah *</label>
    </div>
    <div class="help-info"></div>
</div>