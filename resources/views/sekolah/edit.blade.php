@extends('potongan.main')

@section('title')
	Edit Sekolah {{$sekolah->nama}}
@stop

@section('content')
	<div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Edit Sekolah {{$sekolah->nama}}
                        <small>{{$sekolah->alamat}}</small>
                    </h2>
                </div>
                <div class="body">
                    {{ Form::model($sekolah, array('method' => 'PATCH', 'url' => route('sekolah.update', $sekolah->id), 'files' => true)) }}
                    
                    	@include('sekolah._form')

                        <br>
                        <div class="text-center">
                            <button type="reset" class="btn btn-primary m-t-15 waves-effect">Reset</button>
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">Simpan</button>
                            <a href="{{ route('sekolah.index') }}" class="btn btn-primary m-t-15 waves-effect">Back</a>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <!-- Jquery Validation Plugin Css -->
    <script src="{{ asset('template/plugins/jquery-validation/jquery.validate.js') }}"></script>

    <!-- JQuery Steps Plugin Js -->
    <script src="{{ asset('template/plugins/jquery-steps/jquery.steps.js') }}"></script>

    <!-- Sweet Alert Plugin Js -->
    <script src="{{ asset('template/plugins/sweetalert/sweetalert.min.js') }}"></script>

	<script src="{{ asset('template/js/pages/forms/form-validation.js') }}"></script>
	
@stop

@section('style')
	<link href="{{ asset('template/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" />
@stop
