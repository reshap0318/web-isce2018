@extends('potongan.main')
	
@section('title')
	Role User
@stop

@section('content')
 <!-- Exportable Table -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Role Table
                </h2>
                <ul class="header-dropdown m-r--5">
                    <a href="{{ route('role-akun.create') }}" class="btn bg-blue-grey waves-effect" class=" text-center">Add New Role
                	</a>
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable js-exportable no-footer" id="tbluser">
                        <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Slug</th>
                                <th class="text-center">Peserta</th>
                                <th class="text-center">Edit</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Slug</th>
                                <th class="text-center">Peserta</th>
                                <th class="text-center">Edit</th>
                            </tr>
                        </tfoot>
                        <tbody>
                        	<?php $no=0?>
                        	@foreach($roles as $role)
                                <tr>
                                    <td class="text-center">{{++$no}}</td>
                                    <td><a href="{{ route('role-akun.show',$role->slug) }}"> {{$role->slug}}</a></td>
                                    <td><a href="{{ route('role-akun.show',$role->slug) }}"> {{$role->name}}</a></td>
                                    <td>99999++</td>
                                    <td>
                                        <a href="{{ route('role-akun.edit',$role->slug) }}" class="btn btn-info btn-xs waves-effect">Edit</a>
                                        <a href="{{ route('role-akun.permissions',$role->slug) }}" class="btn btn-info btn-xs waves-effect">Permissions</a>
                                        {!! Form::open(['method'=>'DELETE', 'route' => ['role-akun.destroy', $role->id], 'style' => 'display:inline']) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs waves-effect','id'=>'delete-confirm']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# Exportable Table -->
@stop

@section('script')
	<script src="{{ asset('template/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
    <script type="text/javascript">
        $('.js-exportable').DataTable({
            'info' : false,
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'copy', 'csv', 'excel', 'pdf',
            ]
        });
    </script>
@stop

@section('style')
	<link href="{{ asset('template/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet" />
@stop
