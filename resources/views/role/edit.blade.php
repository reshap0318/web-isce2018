@extends('potongan.main')

@section('title')
	Edit Role User
@stop

@section('content')
	<div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Edit Role User
                        <small></small>
                    </h2>
                </div>
                <div class="body">
                    {{ Form::model($role, array('method' => 'PATCH', 'url' => route('role-akun.update', $role->id), 'files' => true)) }}
                    	@include('role._form')

                        <br>
                        <button type="reset" class="btn btn-primary m-t-15 waves-effect">Reset</button>
                        <button type="submit" class="btn btn-primary m-t-15 waves-effect">Simpan</button>
                        <button type="button" class="btn btn-primary m-t-15 waves-effect">back</button>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <!-- Jquery Validation Plugin Css -->
    <script src="{{ asset('template/plugins/jquery-validation/jquery.validate.js') }}"></script>

    <!-- JQuery Steps Plugin Js -->
    <script src="{{ asset('template/plugins/jquery-steps/jquery.steps.js') }}"></script>

    <!-- Sweet Alert Plugin Js -->
    <script src="{{ asset('template/plugins/sweetalert/sweetalert.min.js') }}"></script>

	<script src="{{ asset('template/js/pages/forms/form-validation.js') }}"></script>
	
@stop

@section('style')
	<link href="{{ asset('template/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" />
@stop
