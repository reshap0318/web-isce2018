@extends('potongan.main')

@section('title')
	Give Permissions Role User
@stop

@section('content')
	<div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Give Permissions Role User
                        <small></small>
                    </h2>
                </div>
                <div class="body">
                    {{ Form::open(array('url' => route('role-akun.simpan',$role->id),'files' => true)) }}
                    	<div class="row">
                    		@foreach($actions as $action)
	                    		<div class="col-md-4">
	                    			<?php 
					                	$first= array_values($action)[0];
					                    $firstname =explode(".", $first)[0];
					                ?> 
		                            <p>
		                                <b>{{Form::label($firstname, $firstname)}}</b>
		                            </p>
		                            <select name="permissions[]" class="form-control show-tick" multiple data-selected-text-format="count">
		                                @foreach($action as $act)
				                            @if(explode(".", $act)[0]=="api")
				                                <option value="{{$act}}" {{array_key_exists($act, $role->permissions)?"selected":""}}>
				                                {{isset(explode(".", $act)[2])?explode(".", $act)[1].".".explode(".", $act)[2]:explode(".", $act)[1]}}</option>
				                            @else
				                                <option value="{{$act}}" {{array_key_exists($act, $role->permissions)?"selected":""}}>

				                                {{explode(".", $act)[1]}}
				      
				                                </option>
				                            @endif
				                        @endforeach
		                            </select>
		                        </div>
		                    @endforeach
                    	</div>
                    	<div class="text-center">
	                        <button type="reset" class="btn btn-primary m-t-15 waves-effect">Reset</button>
	                        <button type="submit" class="btn btn-primary m-t-15 waves-effect">Simpan</button>
	                        <a href="{{ route('role-akun.index') }}" class="btn btn-primary m-t-15 waves-effect" >Back</a>
                    	</div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <!-- Jquery Validation Plugin Css -->
    <script src="{{ asset('template/plugins/jquery-validation/jquery.validate.js') }}"></script>

    <!-- JQuery Steps Plugin Js -->
    <script src="{{ asset('template/plugins/jquery-steps/jquery.steps.js') }}"></script>

    <!-- Sweet Alert Plugin Js -->
    <script src="{{ asset('template/plugins/sweetalert/sweetalert.min.js') }}"></script>

	<script src="{{ asset('template/js/pages/forms/form-validation.js') }}"></script>

    <!-- Multi Select Plugin Js -->
    <script src="{{ asset('template/plugins/multi-select/js/jquery.multi-select.js') }}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ asset('template/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
{{-- 
    <script src="{{ asset('template/js/pages/forms/advanced-form-elements.js') }}"></script>
	 --}}
@stop

@section('style')

	<!-- Multi Select Css -->
    <link href="{{ asset('template/plugins/multi-select/css/multi-select.css') }}" rel="stylesheet">

    <!-- Bootstrap Select Css -->
    <link href="{{ asset('template/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />

	<link href="{{ asset('template/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" />
@stop
