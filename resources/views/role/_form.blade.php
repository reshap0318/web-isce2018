<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('slug', null, ['class'=>'form-control','maxlength'=>'30', 'minlength'=>'3', 'required']) !!}
        <label class="form-label">Slug</label>
    </div>
    <div class="help-info">Min. 3, Max. 30 characters</div>
</div>
<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('name', null, ['class'=>'form-control','maxlength'=>'30', 'minlength'=>'3', 'required']) !!}
        <label class="form-label">Name</label>
    </div>
    <div class="help-info">Min. 3, Max. 30 characters</div>
</div>