<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('nama_prov', null, ['class'=>'form-control','maxlength'=>'30', 'minlength'=>'3']) !!}
        <label class="form-label">Nama Provinsi</label>
    </div>
    <div class="help-info">Min. 3, Max. 30 characters</div>
</div>