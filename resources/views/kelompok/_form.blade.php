
<div class="form-group">
        <p>
            <b>Ketua</b>
        </p>
        {!! Form::select('ketua_id', $user ,null,['class'=>'form-control show-tick', 'required', 'data-live-search'=>'true']) !!}
</div>
<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('nama', null, ['class'=>'form-control','maxlength'=>'16', 'minlength'=>'3']) !!}
        <label class="form-label">Nama Kelompok</label>
    </div>
    <div class="help-info">Min. 3, Max. 16 characters</div>
</div>
