@extends('potongan.main')

@section('title')
	Kelompok Lomba
@stop

@section('style')

	<link href="{{ asset('template/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet" />

@stop
@section('content')
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Kelompok Perlombaan
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <a href="{{ route('kelompok.create') }}" class="btn bg-blue-grey waves-effect" class=" text-center">Add Kelompok Perlombaan
                    	</a>
                    </ul>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable no-footer" id="tbluser">
                            <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">Ketua Tim</th>
                                    <th class="text-center">Nama Kelompok</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">Ketua Tim</th>
                                    <th class="text-center">Nama Kelompok</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </tfoot>
                            <tbody>
                            	<?php $no=0?>
                            	@foreach($kelompoks as $kelompok)
	                                <tr>
	                                    <td class="text-center">{{++$no}}</td>
                                        <td>{{$kelompok->ketua->nama}}</td>
                                        <td>{{$kelompok->nama}}</td>
	                                    <td>
                                            <a href="{{ route('kelompok.edit',$kelompok->id) }}" class="btn btn-info btn-xs waves-effect">Edit</a>
                                            {!! Form::open(['method'=>'DELETE', 'route' => ['kelompok.destroy', $kelompok->id], 'style' => 'display:inline']) !!}
                                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs waves-effect','id'=>'delete-confirm"']) !!}
                                            {!! Form::close() !!}
                                        </td>
	                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
@stop

@section('script')
	
    <script src="{{ asset('template/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('template/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
    <script type="text/javascript">
        $('.js-exportable').DataTable({
            'info' : false,
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'copy', 'csv', 'excel', 'pdf',
            ]
        });

        $("input#delete-confirm").on("click", function(){
            return confirm("Apakah Kamu Ingin Menghapus universitas Ini?");
        });
    </script>
    
@stop