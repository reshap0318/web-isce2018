<!DOCTYPE HTML>
<!--
    Concept by gettemplates.co
    Twitter: http://twitter.com/gettemplateco
    URL: http://gettemplates.co
-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ISCE</title>
        <link rel="icon" href="{{ asset('Alogo.png') }}" type="image/x-icon">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Free HTML5 Website Template by gettemplates.co" />
        <meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
        <meta name="author" content="gettemplates.co" />

        <!-- Facebook and Twitter integration -->
        <meta property="og:title" content=""/>
        <meta property="og:image" content=""/>
        <meta property="og:url" content=""/>
        <meta property="og:site_name" content=""/>
        <meta property="og:description" content=""/>
        <meta name="twitter:title" content="" />
        <meta name="twitter:image" content="" />
        <meta name="twitter:url" content="" />
        <meta name="twitter:card" content="" />

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">


        <!-- Animate.css -->
        <link rel="stylesheet" href="{{ asset('awal/css/animate.css') }}">
        <!-- Icomoon Icon Fonts-->
        <link rel="stylesheet" href="{{ asset('awal/css/icomoon.css') }}">
        <!-- Bootstrap  -->
        <link rel="stylesheet" href="{{ asset('awal/css/bootstrap.css') }}">

        <!-- Magnific Popup -->
        <link rel="stylesheet" href="{{ asset('awal/css/magnific-popup.css') }}">

        <!-- Theme style  -->
        <link rel="stylesheet" href="{{ asset('awal/css/style.css') }}">

        <!-- Modernizr JS -->
        <script src="{{ asset('awal/js/modernizr-2.6.2.min.js') }}"></script>
        <!-- FOR IE9 below -->
        <!--[if lt IE 9]>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

    <div class="fh5co-loader"></div>

    <div id="page">
    <nav class="fh5co-nav" role="navigation">
        <div class="container">
            <div class="row">
                <div class="col-xs-2 text-left">
                    <div id="fh5co-logo"><a href="{{url('/')}}">ISCE<span>.</span></a></div>
                </div>
                <div class="col-xs-10 text-right menu-1">
                    <ul>
                        <li><a href="{{url('/')}}">Beranda</a></li>
                        <li><a href="#">Pengumuman</a></li>
                        <li><a href="{{url('faq')}}">FAQ</a></li>
                        <li><a href="http://hmsiunand.com/">Tentang</a></li>
                        @if (Sentinel::getUser())
                            <li><a href="{{url('dashboard')}}">Dashboard</a></li>
                            <li>
                                <a href="{{ url('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Log Out
                                </a>
                            </li>
                            <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                                  @csrf
                            </form>
                        @else
                            <li class="active"><a href="{{url('login')}}">Masuk</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    @include('vendor.toast.messages-jquery')

    <header id="fh5co-header" class="fh5co-cover" role="banner" style="background-image:url({{ asset('awal/images/img_bg_1.jpg') }});" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-7 text-left">
                    <div class="display-t">
                        <div class="display-tc animate-box" data-animate-effect="fadeInUp">
                            <h1 class="mb30">Login</h1>
                            <form id="sign_in" method="POST" action="{{url('login')}}">
                                {{ csrf_field() }}
                                  <div class="form-control">
                                    <label for="" class="col-md-3">Username</label>
                                    <input type="text" name="username" value="" placeholder="Username" required>
                                  </div>
                                  <div class="form-control">
                                    <label for="" class="col-md-3">Password</label>
                                    <input type="password" name="password" value="" placeholder="Password" required>
                                  </div>
                                  <input type="submit" name="" value="Login" class="btn btn-primary">
                                  <em class="or">or</em>
                                  <a href="{{url('daftar')}}" class="link-watch">Register</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <footer id="fh5co-footer" role="contentinfo">
        <div class="container">
            <div class="row row-pb-md">
                <div class="col-md-4 fh5co-widget ">
                    <h3>Information System Celebration Event</h3>
                    <p>Event perayaan berdirinya Jurusan Sistem Informasi Universitas Andalas.</p>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1 ">
                    <ul class="fh5co-footer-links">
                        <li><a href="announcement.html">Pengumuman</a></li>
                        <li><a href="faq.html">FAQ</a></li>
                        <li><a href="http://hmsiunand.com/">Tentang</a></li>
                    </ul>
                </div>
            </div>

            <div class="row copyright">
                <div class="col-md-12 text-center">
                    <p>
                        <small class="block">&copy; 2018 Himpunan Mahasiswa Sistem Informasi Universitas Andalas.</small>
                    </p>
                </div>
            </div>

        </div>
    </footer>
    </div>

    <div class="gototop js-top">
        <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
    </div>

    <!-- jQuery -->
    <script src="{{ asset('awal/js/jquery.min.js') }}"></script>
    <!-- jQuery Easing -->
    <script src="{{ asset('awal/js/jquery.easing.1.3.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('awal/js/bootstrap.min.js') }}"></script>
    <!-- Waypoints -->
    <script src="{{ asset('awal/js/jquery.waypoints.min.js') }}"></script>
    <!-- countTo -->
    <script src="{{ asset('awal/js/jquery.countTo.js') }}"></script>
    <!-- Magnific Popup -->
    <script src="{{ asset('awal/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('awal/js/magnific-popup-options.js') }}"></script>
    <!-- Stellar -->
    <script src="{{ asset('awal/js/jquery.stellar.min.js') }}"></script>
    <!-- Main -->
    <script src="{{ asset('awal/js/main.js') }}"></script>

    </body>
</html>
