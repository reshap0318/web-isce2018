<!DOCTYPE HTML>
<!--
    Concept by gettemplates.co
    Twitter: http://twitter.com/gettemplateco
    URL: http://gettemplates.co
-->
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ISCE</title>
        <link rel="icon" href="{{ asset('Alogo.png') }}" type="image/x-icon">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Free HTML5 Website Template by gettemplates.co" />
        <meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
        <meta name="author" content="gettemplates.co" />

        <!-- Facebook and Twitter integration -->
        <meta property="og:title" content=""/>
        <meta property="og:image" content=""/>
        <meta property="og:url" content=""/>
        <meta property="og:site_name" content=""/>
        <meta property="og:description" content=""/>
        <meta name="twitter:title" content="" />
        <meta name="twitter:image" content="" />
        <meta name="twitter:url" content="" />
        <meta name="twitter:card" content="" />

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">


        <!-- Animate.css -->
        <link rel="stylesheet" href="{{ asset('awal/css/animate.css') }}">
        <!-- Icomoon Icon Fonts-->
        <link rel="stylesheet" href="{{ asset('awal/css/icomoon.css') }}">
        <!-- Bootstrap  -->
        <link rel="stylesheet" href="{{ asset('awal/css/bootstrap.css') }}">

        <!-- Magnific Popup -->
        <link rel="stylesheet" href="{{ asset('awal/css/magnific-popup.css') }}">

        <!-- Theme style  -->
        <link rel="stylesheet" href="{{ asset('awal/css/style.css') }}">

        <!-- Modernizr JS -->
        <script src="{{ asset('awal/js/modernizr-2.6.2.min.js') }}"></script>
        <!-- FOR IE9 below -->
        <!--[if lt IE 9]>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

    <div class="fh5co-loader"></div>

    <div id="page">
    <nav class="fh5co-nav" role="navigation">
        <div class="container">
            <div class="row">
                <div class="col-xs-2 text-left">
                    <div id="fh5co-logo"><a href="{{url('/')}}">ISCE<span>.</span></a></div>
                </div>
                <div class="col-xs-10 text-right menu-1">
                    <ul>
                        <li class="active"><a href="{{url('/')}}">Beranda</a></li>
                        <li><a href="#">Pengumuman</a></li>
                        <li><a href="{{url('faq')}}">FAQ</a></li>
                        <li><a href="http://hmsiunand.com/">Tentang</a></li>
                        @if (Sentinel::getUser())
                            <li><a href="{{url('dashboard')}}">Dashboard</a></li>
                            <li>
                                <a href="{{ url('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Log Out
                                </a>
                            </li>
            <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                  @csrf
            </form>
                        @else
                            <li><a href="{{url('login')}}">Masuk</a></li>
                        @endif
                        
                    </ul>
                </div>
            </div>

        </div>
    </nav>

    <header id="fh5co-header" class="fh5co-cover" role="banner" style="background-image:url({{ asset('awal/images/img_bg_1.jpg') }});" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-7 text-left">
                    <div class="display-t">
                        <div class="display-tc animate-box" data-animate-effect="fadeInUp">
                            <h1 class="mb30">Information System Celebration Event</h1>
                            <p>
                                <a href="{{url('login')}}" class="btn btn-primary">Login</a>  <em class="or">or</em>
                                <a href="{{url('daftar')}}" class="link-watch">Register</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div id="fh5co-project">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 fh5co-project animate-box" data-animate-effect="fadeIn">
                    <a href="#"><img src="{{ asset('awal/images/announcement.png') }}" class="img-responsive">
                        <div class="fh5co-copy">
                            <h3>Pengumuman</h3>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 col-sm-6 fh5co-project animate-box" data-animate-effect="fadeIn">
                    <a href="http://hmsiunand.com/"><img src="{{ asset('awal/images/information.png') }}" alt="Free HTML5 Website Template by gettemplates.co" class="img-responsive">
                        <div class="fh5co-copy">
                            <h3>Tentang</h3>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 col-sm-6 fh5co-project animate-box" data-animate-effect="fadeIn">
                    <a style="height: 100%; width: 100%" href="{{url('faq')}}"><img src="{{ asset('awal/images/faq.png') }}" alt="Free HTML5 Website Template by gettemplates.co" class="img-responsive">
                        <div class="fh5co-copy">
                            <h3>FAQ</h3>
                        </div>
                    </a>
                </div>

            </div>
        </div>

    </div>



    <div id="fh5co-services" class="fh5co-bg-section border-bottom">
        <div class="container">
            <div class="row row-pb-md">
                <div class="text-center animate-box" data-animate-effect="fadeInUp">
                    <div class="fh5co-heading">
                        <h2>Kategori Kegiatan</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6 ">
                    <div class="feature-center animate-box" data-animate-effect="fadeInUp">
                        <a href="#">
                            <span class="icon">
                                <i class="icon-classic-computer"></i>
                            </span>
                            <h3>Olimpiade Komputer</h3>
                            <p style="color: grey;">Kegiatan olimpiade komputer untuk siswa/i tingkat SMA se-Sumatra Barat yang diadakan dengan dua tahap yakni tahap penyisihan pada 5 November 2018 dan tahap final pada 6 November 2018.</p>
                        </a>
                        <a href="{{url('foto/panduan/PROPOSALOLKOM.pdf')}}" class="btn" download="PROPOSALOLKOM.pdf">Download Panduan</a>
                        <a href="{{url('olimpiade-Komputer')}}" class="btn">Detail</a>
                        <a href="{{url('daftar')}}" class="btn">Daftar</a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 ">
                    <div class="feature-center animate-box" data-animate-effect="fadeInUp">
                        <a href="#">
                            <span class="icon">
                                <i class="icon-open-book"></i>
                            </span>
                            <h3>Seminar Nasional</h3>
                            <p style="color: grey;">Seminar nasional ini akan memperkenalkan dunia entrepreneur dikalangan umum terutama mahasiswa Universitas Andalas dengan mendatangkan tokoh yang berperan didalam entrepreneur itu sendiri.</p>
                        </a> 
                        <a href="#" class="btn">Download Panduan</a>
                        <a href="#" class="btn">Detail</a>
                        <a href="{{url('daftar')}}" class="btn">Daftar</a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 ">
                    <div class="feature-center animate-box" data-animate-effect="fadeInUp">
                        <a href="#">
                            <span class="icon">
                                <i class="icon-code"></i>
                            </span>
                            <h3>Hackathon</h3> 
                            <p style="color: grey;">Hackathon dengan cakupan peserta nasional akan berakhir dengan presentasi 5 tim yang lolos ke babak final pada 9 November 2018. Kegiatan ini dilaksanakan untuk melihat seberapa besar kemampuan dalam ngoding.</p>
                        </a>
                        <a href="{{url('foto/panduan/PROPOSALHACKATHON.pdf')}}" download="PROPOSALHACKATHON.pdf" class="btn">Download Panduan</a>
                        <a href="{{url('hackathon')}}" class="btn">Detail</a>
                        <a href="#" class="btn">Daftar</a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 ">
                    <div class="feature-center animate-box" data-animate-effect="fadeInUp">
                        <a href="#">
                            <span class="icon">
                                <i class="icon-eye"></i>
                            </span>
                            <h3>Expo & Bazar</h3>
                            <p style="color: grey;">Kegiatan ini berlangsung selama ISCE berlangsung dari hari senin sampai jumat. Kegiatan ini akan memberikan suguhan seperti akustik serta expo dan bazar hal-hal menarik.</p>
                        </a>
                        <a href="#" class="btn">Download Panduan</a>
                        <a href="#" class="btn">Detail</a>
                        <a href="#" class="btn">Daftar</a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <footer id="fh5co-footer" role="contentinfo">
        <div class="container">
            <div class="row row-pb-md">
                <div class="col-md-4 fh5co-widget ">
                    <h3>Information System Celebration Event</h3>
                    <p>Event perayaan berdirinya Jurusan Sistem Informasi Universitas Andalas.</p>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1 ">
                    <ul class="fh5co-footer-links">
                        <li><a href="#">Pengumuman</a></li>
                        <li><a href="{{url('faq')}}">FAQ</a></li>
                        <li><a href="http://hmsiunand.com/">Tentang</a></li>
                    </ul>
                </div>
            </div>

            <div class="row copyright">
                <div class="col-md-12 text-center">
                    <p>
                        <small class="block">&copy; 2018 Himpunan Mahasiswa Sistem Informasi Universitas Andalas.</small>
                    </p>
                </div>
            </div>

        </div>
    </footer>
    </div>

    <div class="gototop js-top">
        <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
    </div>

    <!-- jQuery -->
    <script src="{{ asset('awal/js/jquery.min.js') }}"></script>
    <!-- jQuery Easing -->
    <script src="{{ asset('awal/js/jquery.easing.1.3.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('awal/js/bootstrap.min.js') }}"></script>
    <!-- Waypoints -->
    <script src="{{ asset('awal/js/jquery.waypoints.min.js') }}"></script>
    <!-- countTo -->
    <script src="{{ asset('awal/js/jquery.countTo.js') }}"></script>
    <!-- Magnific Popup -->
    <script src="{{ asset('awal/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('awal/js/magnific-popup-options.js') }}"></script>
    <!-- Stellar -->
    <script src="{{ asset('awal/js/jquery.stellar.min.js') }}"></script>
    <!-- Main -->
    <script src="{{ asset('awal/js/main.js') }}"></script>

    </body>
</html>
