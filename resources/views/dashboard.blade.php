@extends('potongan.main')

@section('title')
    Dashboard User {{Sentinel::getuser()->nama}}
@stop

@section('style')

    <link href="{{ asset('template/plugins/light-gallery/css/lightgallery.css') }}" rel="stylesheet" />

@stop
@section('content')
<!-- Dismissible Alerts -->
    <div class="row clearfix">

        @if($pembayaran=="x")
        {{-- pembayaran --}}
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                Anda Belum Melakukan Pembayaran, Silakan Lakukan Pembayaran dan Kirim Bukti Pembayaran Anda <a href="{{route('berkas.index',[Sentinel::getuser()->nama, Sentinel::getuser()->id])}}">disini</a>
            </div>
        </div>
        @endif

        @if($pes=="x")
        {{-- pembayaran --}}
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                Anda Belum Memiliki Kelompok, Silakan Buat Kelompok atau Invited Akun Anda Oleh Ketua Tim Anda.
            </div>
        </div>
        @endif

        
        @if($ktm=="x")
        {{-- ktm --}}
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                Anda Belum Memasukan Kartu KTM Anda <a href="{{route('berkas.index',[Sentinel::getuser()->nama, Sentinel::getuser()->id])}}">disini</a>
            </div>
        </div>
        @endif

        @if($foto=="x" || $ktm=="x" || $nim=="x" || $universitas=="x" || $jurusan=="x" || $nisn=="x" || $sekolah=="x")
        {{-- ktm --}}
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                Data Anda Belum Lengkap, Silakan Lengkapi Data Anda di menu profil
            </div>
        </div>
        @endif
    </div>
<!-- #END# Dismissible Alerts -->
    <div class="row clearfix">
        <!-- With Captions -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="body">
                    <div id="carousel-example-generic_2" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic_2" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic_2" data-slide-to="1"></li>
                            {{-- <li data-target="#carousel-example-generic_2" data-slide-to="2"></li> --}}
                        </ol>
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="{{url('foto/pengumuman/hachathon.png')}}" />
                                <div class="carousel-caption">
                                    <h3>Informasi Waktu Penting Hackathon</h3>
                                    <p>Jangan Lupa waktu - Waktu Penting Hackathon</p>
                                </div>
                            </div>
                            <div class="item">
                                <img src="{{url('foto/pengumuman/hachathon.png')}}" />
                                <div class="carousel-caption">
                                    <h3>Informasi Waktu Penting Hackathon</h3>
                                    <p>Jangan Lupa waktu - Waktu Penting Hackathon</p>
                                </div>
                            </div>
                        </div>
                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic_2" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic_2" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# With Captions -->
    </div>


@stop

@section('script')
    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset('template/plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>


    <script src="{{ asset('template/plugins/light-gallery/js/lightgallery-all.js') }}"></script>

@stop