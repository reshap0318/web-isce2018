@if (count($errors) > 0)
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header bg-red">
                <h2>
                    Upssss !! <small>There is an error...</small>
                </h2>
            </div>
            <div class="body">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endif

@include('vendor.toast.messages-jquery')