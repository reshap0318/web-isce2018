<div class="user-info">
    <div class="image">
        @if(Sentinel::getuser()->foto)
            <img width="48" height="48" class="" src="{{url('foto/foto/'.Sentinel::getuser()->foto)}}" alt="User">
        @else
            <img width="48" height="48" class="" src="{{url('foto/foto/'.$user->foto)}}" alt="User">
        @endif
        {{-- <img src="{{ asset('template/images/user.png') }}" width="48" height="48" alt="User" /> --}}
    </div>
    <div class="info-container">
        <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Sentinel::getuser()->nama }}</div>
        <div class="email">{{ Sentinel::getuser()->email }}</div>
        <div class="btn-group user-helper-dropdown">
            <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
            <ul class="dropdown-menu pull-right">
                <li><a href="{{url('profil/'.Sentinel::getuser()->nama.'/'. Sentinel::getuser()->id)}}"><i class="material-icons">person</i>Profile</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="{{url('password')}}"><i class="material-icons">group</i>Ganti Password</a></li>
                <li role="separator" class="divider"></li>
                <li>
                    <a href="{{ url('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="material-icons">input</i>Sign Out</a>
                </li>
                <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                  @csrf
              </form>
            </ul>
        </div>
    </div>
</div>