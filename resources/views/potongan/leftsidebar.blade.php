<aside id="leftsidebar" class="sidebar"> 
    <!-- User Info -->
    @include('potongan.userinfo')
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu" id="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>
            <li>
                <a href="{{url('dashboard')}}">
                    <i class="material-icons">home</i>
                    <span>Home</span>
                </a>
            </li>
            @if (Sentinel::getUser()->hasAccess(['berkas.index']))
            <li>
                <a href="{{route('berkas.index',[Sentinel::getuser()->nama, Sentinel::getuser()->id])}}">
                    <i class="material-icons">insert_drive_file</i>
                    <span>Berkas</span>
                </a>
            </li>
            @endif
            @if (Sentinel::getUser()->hasAccess(['hackathon.index']))
            <li>
                <a href="{{route('hackathon.index',[Sentinel::getuser()->nama, Sentinel::getuser()->id])}}">
                    <i class="material-icons">code</i>
                    <span>Hackathon</span>
                </a>
            </li>
            @endif
            @if (Sentinel::getUser()->hasAccess(['pemilik-akun.index','role-akun.index','sekolah.index','pelajar-profil.index','universitas.index','mahasiswa.index','kelompok.index','kegiatan.index','user-lomba.index','provinsi.index','kabupaten-kota.index','kecamatan.index']))
            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">storage</i>
                    <span>Management</span>
                </a>
                <ul class="ml-menu">
                    @if (Sentinel::getUser()->hasAccess(['pemilik-akun.index']))
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <span>User Management</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{ route('pemilik-akun.index') }}">
                                    <span>All User</span>
                                </a>
                            </li>
                            @if (Sentinel::getUser()->hasAccess(['pemilik-akun.create']))
                            <li>
                                <a href="{{ route('pemilik-akun.create') }}">
                                    <span>Create User</span>
                                </a>
                            </li>
                            @endif
                        </ul>
                    </li>
                    @endif
                    @if (Sentinel::getUser()->hasAccess(['role-akun.index']))
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <span>Role Management</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{ route('role-akun.index') }}">
                                    <span>All Role</span>
                                </a>
                            </li>
                            @if (Sentinel::getUser()->hasAccess(['role-akun.create']))
                            <li>
                                <a href="{{ route('role-akun.create') }}">
                                    <span>Create Role</span>
                                </a>
                            </li>
                            @endif
                        </ul>
                    </li>
                    @endif
                    @if (Sentinel::getUser()->hasAccess(['sekolah.index']))
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <span>Sekolah Management</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{ route('sekolah.index') }}">
                                    <span>All Sekolah</span>
                                </a>
                            </li>
                            @if (Sentinel::getUser()->hasAccess(['sekolah.create']))
                            <li>
                                <a href="{{ route('sekolah.create') }}">
                                    <span>Create Sekolah</span>
                                </a>
                            </li>
                            @endif
                        </ul>
                    </li>
                    @endif
                    @if (Sentinel::getUser()->hasAccess(['pelajar-profil.index']))
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <span>Pelajar Requirment</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{ route('pelajar-profil.index') }}">
                                    <span>All Pelajar Requirment</span>
                                </a>
                            </li>
                            @if (Sentinel::getUser()->hasAccess(['pelajar-profil.create']))
                            <li>
                                <a href="{{ route('pelajar-profil.create') }}">
                                    <span>Create Pelajar Requirment</span>
                                </a>
                            </li>
                            @endif
                        </ul>
                    </li>
                    @endif
                    @if (Sentinel::getUser()->hasAccess(['universitas.index']))
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <span>Universitas Management</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{ route('universitas.index') }}">
                                    <span>All Universitas</span>
                                </a>
                            </li>
                            @if (Sentinel::getUser()->hasAccess(['universitas.create']))
                            <li>
                                <a href="{{ route('universitas.create') }}">
                                    <span>Create Universitas</span>
                                </a>
                            </li>
                            @endif
                        </ul>
                    </li>
                    @endif
                    @if (Sentinel::getUser()->hasAccess(['mahasiswa.index']))
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <span>Mahasiswa Requirment</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{ route('mahasiswa.index') }}">
                                    <span>All Mahasiswa Requirment</span>
                                </a>
                            </li>
                            @if (Sentinel::getUser()->hasAccess(['mahasiswa.create']))
                            <li>
                                <a href="{{ route('mahasiswa.create') }}">
                                    <span>Create Mahasiswa Requirment</span>
                                </a>
                            </li>
                            @endif
                        </ul>
                    </li>
                    @endif
                    @if (Sentinel::getUser()->hasAccess(['kelompok.index']))
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <span>Kelompok</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{ route('kelompok.index') }}">
                                    <span>All Kelompok</span>
                                </a>
                            </li>
                            @if (Sentinel::getUser()->hasAccess(['kelompok.create']))
                            <li>
                                <a href="{{ route('kelompok.create') }}">
                                    <span>Create Kelompok</span>
                                </a>
                            </li>
                            @endif
                        </ul>
                    </li>
                    @endif
                    @if (Sentinel::getUser()->hasAccess(['kegiatan.index']))
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <span>Kegiatan</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{ route('kegiatan.index') }}">
                                    <span>All Kegiatan</span>
                                </a>
                            </li>
                            @if (Sentinel::getUser()->hasAccess(['kegiatan.create']))
                            <li>
                                <a href="{{ route('kegiatan.create') }}">
                                    <span>Create Kegiatan</span>
                                </a>
                            </li>
                            @endif
                        </ul>
                    </li>
                    @endif
                    @if (Sentinel::getUser()->hasAccess(['user-lomba.index']))
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <span>User Lomba</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{ route('user-lomba.index') }}">
                                    <span>All Pesera</span>
                                </a>
                            </li>
                            @if (Sentinel::getUser()->hasAccess(['user-lomba.create']))
                            <li>
                                <a href="{{ route('user-lomba.create') }}">
                                    <span>Create Peserta</span>
                                </a>
                            </li>
                            @endif
                        </ul>
                    </li>
                    @endif
                    @if (Sentinel::getUser()->hasAccess(['provinsi.index','kabupaten-kota.index','kecamatan.index']))
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <span>Alamat Management</span>
                        </a>
                        <ul class="ml-menu">
                            @if (Sentinel::getUser()->hasAccess(['provinsi.index']))
                            <li>
                                <a href="javascript:void(0);" class="menu-toggle">
                                    <span>Provinsi</span>
                                </a>
                                <ul class="ml-menu">
                                    <li>
                                        <a href="{{route('provinsi.index')}}">
                                            <span>All Provinsi</span>
                                        </a>
                                    </li>
                                    @if (Sentinel::getUser()->hasAccess(['provinsi.create']))
                                    <li>
                                        <a href="{{route('provinsi.create')}}">
                                            <span>Create Provinsi</span>
                                        </a>
                                    </li>
                                    @endif
                                </ul>
                            </li>
                            @endif
                            @if (Sentinel::getUser()->hasAccess(['kabupaten-kota.index']))
                            <li>
                                <a href="javascript:void(0);" class="menu-toggle">
                                    <span>Kab & kota</span>
                                </a>
                                <ul class="ml-menu">
                                    <li>
                                        <a href="{{route('kabupaten-kota.index')}}">
                                            <span>All Kab & Kota</span>
                                        </a>
                                    </li>
                                    @if (Sentinel::getUser()->hasAccess(['kabupaten-kota.create']))
                                    <li>
                                        <a href="{{route('kabupaten-kota.create')}}">
                                            <span>Create Kab & Kota</span>
                                        </a>
                                    </li>
                                    @endif
                                </ul>
                            </li>
                            @endif
                            @if (Sentinel::getUser()->hasAccess(['kecamatan.index']))
                            <li>
                                <a href="javascript:void(0);" class="menu-toggle">
                                    <span>Kecamatan</span>
                                </a>
                                <ul class="ml-menu">
                                    <li>
                                        <a href="{{route('kecamatan.index')}}">
                                            <span>All Kecamatan</span>
                                        </a>
                                    </li>
                                    @if (Sentinel::getUser()->hasAccess(['kabupaten-kota.create']))
                                    <li>
                                        <a href="{{route('kecamatan.create')}}">
                                            <span>Create Kecamatan</span>
                                        </a>
                                    </li>
                                    @endif
                                </ul>
                            </li>
                            @endif
                        </ul>
                    </li>
                    @endif
                </ul>
            </li>
            @endif
            <li>
                <a href="{{url('bukti-pembayaran')}}">
                    <i class="material-icons">pages</i>
                    <span>Cetak Bukti Pembayaran</span>
                </a>
            </li>
        </ul>
    </div>
</aside>