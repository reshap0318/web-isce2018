<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('username', null, ['class'=>'form-control','maxlength'=>'10', 'minlength'=>'3', 'required']) !!}
        <label class="form-label">Username</label>
    </div>
    <div class="help-info">Min. 3, Max. 10 characters</div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('nama', null, ['class'=>'form-control','maxlength'=>'30', 'minlength'=>'3', 'required']) !!}
        <label class="form-label">Nama</label>
    </div>
    <div class="help-info">Min. 3, Max. 30 characters</div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::email('email', null, ['class'=>'form-control email', 'required']) !!}
        <label class="form-label">Email</label>
    </div>
    <div class="help-info">Email use @</div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('no_hp', null, ['class'=>'form-control mobile-phone-number', 'required']) !!}
        <label class="form-label">NO HP</label>
    </div>
    <div class="help-info">use 0-10</div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::password('password', ['class'=>'form-control','required']) !!}
        <label class="form-label">Password</label>
    </div>
    <div class="help-info"></div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::password('password_confirm', ['class'=>'form-control','required']) !!}
        <label class="form-label">Password Confirm</label>
    </div>
    <div class="help-info"></div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('tempat_lahir', null, ['class'=>'form-control','maxlength'=>'10', 'minlength'=>'3', 'required']) !!}
        <label class="form-label">Tempat Lahir</label>
    </div>
    <div class="help-info"></div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('tanggal_lahir', null, ['class'=>'datepicker form-control', 'required', 'placeholder' => "Tanggal Lahir"]) !!}
    </div>
    <div class="help-info"></div>
</div>

<div class="form-group form-float">
        <input name="jenis_kelamin" type="radio" id="LL" value="LL" class="with-gap radio-col-red"/>
        <label for="LL">LL</label> 
        <input name="jenis_kelamin" type="radio" id="PP" value="PP" class="with-gap radio-col-red"/>
        <label for="PP">PP</label> 

        {!! Form::radio('jenis_kelamin', 'L', ['class'=>'with-gap radio-col-red', 'id' => 'L']) !!}
        <label for="L">Laki - Laki</label> 
        {!! Form::radio('jenis_kelamin', 'P', ['class'=>'with-gap radio-col-red', 'id' => 'P']) !!}
        <label for="P">Perempuan</label> 
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('provinsi', null, ['class'=>'form-control', 'required']) !!}
        <label class="form-label">Provinsi</label>
    </div>
    <div class="help-info"></div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('kab_kota', null, ['class'=>'form-control', 'required']) !!}
        <label class="form-label">Kabupaten & Kota</label>
    </div>
    <div class="help-info"></div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('kecamatan_id', null, ['class'=>'form-control', 'required']) !!}
        <label class="form-label">Kecamatan</label>
    </div>
    <div class="help-info"></div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::textarea('alamat', null, ['class'=>'form-control', 'required']) !!}
        <label class="form-label">Alamat</label>
    </div>
    <div class="help-info"></div>
</div>