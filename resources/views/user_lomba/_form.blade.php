
<div class="form-group">
        <p>
            <b>User</b>
        </p>
        {!! Form::select('user_id', $user ,null,['class'=>'form-control show-tick', 'required', 'data-live-search'=>'true']) !!}
</div>
<div class="form-group">
        <p>
            <b>Kelompok</b>
        </p>
        {!! Form::select('kelompok_id', $kelompok ,null,['class'=>'form-control show-tick', 'required', 'data-live-search'=>'true']) !!}
</div>
<div class="form-group">
        <p>
            <b>Kegiatan</b>
        </p>
        {!! Form::select('kegiatan_id', $kegiatan ,null,['class'=>'form-control show-tick', 'required', 'data-live-search'=>'true']) !!}
</div>
