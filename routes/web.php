<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome'); 
});
Auth::routes();
Route::get('login','loginController@login');
Route::post('login','loginController@masuk');
Route::post('logout','loginController@keluar');
Route::post('daftar','loginController@mendaftar');
Route::get('daftar','loginController@daftar');

Route::group(['middleware' => ['web', 'auth', 'permission'] ], function () {

	Route::get('dashboard', 'berkasController@break')->name('home.dashboard');

	//user
	Route::resource('pemilik-akun','userController');
	//role dan pembagian lomba
	Route::resource('role-akun','RoleController');
	Route::get('permission-akun/{id}','RoleController@permissions')->name('role-akun.permissions');
	Route::post('permission-akun/{id}','RoleController@simpan')->name('role-akun.simpan');

	//provinsi
	Route::resource('provinsi','ProvinsiController');
	//kabupaten - kota
	Route::resource('kabupaten-kota','KabKotaController');
	//kecamatan
	Route::resource('kecamatan','kecamatanController');
	//sekolah
	Route::resource('sekolah','SekolahController');
	//universitas
	Route::resource('universitas','UniversitasController');
	//pelajar-profil
	Route::resource('pelajar-profil','PelajarProfilController');
	//mahasiswa-profil
	Route::resource('mahasiswa','MahasiswaProfilController');
	//kelompok
	Route::resource('kelompok', 'KelompokController');
	//kegiatan
	route::resource('kegiatan', 'KegiatanController');
	//user lomba
	route::resource('user-lomba', 'UserLombaController');

	//berkas-user
	route::get('berkas/{user}/{id}', 'berkasController@index')->name('berkas.index');
	//hackathon-user
	route::get('hackathon/{user}/{id}', 'berkasController@indexhack')->name('hackathon.index');
	//ktm
	route::patch('upload-ktm/{id}', 'berkasController@ktm')->name('hackathon.ktm');
	//foto
	route::patch('upload-foto/{id}', 'berkasController@foto');
	//bayar hackathon
	route::patch('upload-bayar/{id}', 'berkasController@bayar');
	//membuat group
	route::post('hackathon-gruop', 'berkasController@makegrup')->name('hackathon.grup');
	//menambahkan anggota
	route::post('hackathon-anggota', 'berkasController@addanggota')->name('hackathon.addanggota');
	//proposal
	route::post('upload-proposal/{id}', 'berkasController@addproposal')->name('hackathon.proposal');
	route::DELETE('del-proposal/{id}/delete', 'berkasController@dellproposal')->name('hackathon.dellproposal');

	route::get('password', 'userController@password');
	route::patch('gantipassword', 'userController@ganti');

	route::get('profil/{nama}/{id}','userController@profil');
	route::get('editprofil/{nama}/{id}','userController@editprofil');

	route::get('bukti-pembayaran','userController@bukti');
	route::post('perbaruiprofil', 'userController@simpanprofil');
});

	//membuka img
	route::get('foto/{type}/{file_id}', 'berkasController@file');
	route::get('tambah-sekolah', 'SekolahController@sekolah');
	route::get('tambah-universitas', 'UniversitasController@universitas');
	route::post('tambah-universitas', 'UniversitasController@simpanuni');
	route::post('tambah-sekolah', 'SekolahController@simpanseko');
	
	route::get('olimpiade-Komputer','KegiatanController@olkom');
	route::get('hackathon','KegiatanController@hack');
	route::get('faq','KegiatanController@faq');
	route::post('faqt','KegiatanController@faqt');
