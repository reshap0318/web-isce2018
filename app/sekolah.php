<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sekolah extends Model
{
    protected $table = 'sekolahs';

	const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at'; 

     protected $casts = [
        'nama' => 'string',
        'kontak' => 'string',
        'nama_kontak' => 'string',
        'kode_pos' => 'string',
        'kecamatan_id' => 'string',
        'alamat' => 'string',
    ];

    protected $fillable = [
        'nama',
        'kontak',
        'nama_kontak',
        'kode_pos',
        'kecamatan_id',
        'alamat',
    ];

    public function kecamatan()
    {
        return $this->belongsTo(kecamatan::class, 'kecamatan_id', 'id');
    }
}
