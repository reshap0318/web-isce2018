<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kelompok extends Model
{
    protected $table = 'kelompoks';

	const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

     protected $casts = [
        'ketua_id' => 'integer',
        'nama' => 'string',
    ];

    protected $fillable = [
        'ketua_id',
        'nama',
    ];

    public function ketua()
    {
        return $this->belongsTo(user::class, 'ketua_id', 'id');
    }
}
