<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user_lomba extends Model
{
    protected $table = 'user_lombas';  

	const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at'; 

     protected $casts = [
        'user_id' => 'integer',
        'kegiatan_id' => 'integer',
        'kelompok_id' => 'integer',
    ];

    protected $fillable = [
        'user_id',
        'kegiatan_id',
        'kelompok_id',
    ]; 

    public function user()
    {
        return $this->belongsTo(user::class, 'user_id', 'id');
    }

    public function kegiatan()
    {
        return $this->belongsTo(kegiatan::class, 'kegiatan_id', 'id');
    }

    public function kelompok()
    {
        return $this->belongsTo(kelompok::class, 'kelompok_id', 'id');
    }
}
