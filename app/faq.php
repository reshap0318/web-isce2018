<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class faq extends Model
{
    protected $table = 'faqs';

	const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

     protected $casts = [
        'nama' => 'string',
		'email' => 'string',
		'judul' => 'string',
		'pesan' => 'string',
    ];

    protected $fillable = [
        'nama',
		'email',
		'judul',
		'pesan',
    ];
}
