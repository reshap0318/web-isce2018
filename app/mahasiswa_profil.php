<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mahasiswa_profil extends Model
{
    protected $table = 'mahasiswa_profils';

	const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

     protected $casts = [
        'nim'               => 'string',
        'universitas_id'    => 'integer',
        'ktm'               => 'string',
        'jurusan'           => 'string',
        'bukti_pembayaran'  => 'string',
    ];

    protected $fillable = [
        'jurusan',
        'nim',
        'universitas_id',
        'ktm',
        'bukti_pembayaran',
    ];

    public function user()
    {
        return $this->belongsTo(user::class, 'id', 'id');
    }

    public function universitas()
    {
        return $this->belongsTo(universitas::class, 'universitas_id', 'id');
    }
}
