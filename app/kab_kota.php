<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kab_kota extends Model
{
    protected $table = 'kab_kotas';

	const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

     protected $casts = [
        'nama_kab_kota' => 'string',
        'prov_id' => 'integer',
    ];

    protected $fillable = [
        'nama_kab_kota',
        'prov_id',
    ];


    public function provinsi()
    {
        return $this->belongsTo(provinsi::class, 'prov_id', 'id');
    }
}
