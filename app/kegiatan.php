<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kegiatan extends Model
{
	protected $table = 'kegiatans';

	const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

     protected $casts = [
		'nama' 				=> 'string',
		'tema' 				=> 'string',
		'waktu_mulai' 		=> 'string',
		'waktu_berakhir' 	=> 'string',
		'insert' 			=> 'string',
		'lokasi' 			=> 'string',
		'latar_belakang' 	=> 'string',
		'tujuan' 			=> 'string',
		'deskripsi' 		=> 'string',
		'foto' 				=> 'string',
    ];

    protected $fillable = [
		'nama',
		'tema',
		'waktu_mulai',
		'waktu_berakhir',
		'insert',
		'lokasi',
		'latar_belakang',
		'tujuan',
		'deskripsi',
		'foto',
    ];
}
