<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class provinsi extends Model
{
    protected $table = 'provinsis';

	const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

     protected $casts = [
        'nama_prov' => 'string',
    ];

    protected $fillable = [
        'nama_prov',
    ];
}
