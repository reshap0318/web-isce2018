<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class universitas extends Model
{
    protected $table = 'universitas'; 

	const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at'; 

     protected $casts = [
        'nama' => 'string',
        'kode_pos' => 'string',
        'kecamatan_id' => 'string',
        'alamat' => 'string',
    ];

    protected $fillable = [
        'nama',
        'kode_pos',
        'kecamatan_id',
        'alamat',
    ];

    public function kecamatan()
    {
        return $this->belongsTo(kecamatan::class, 'kecamatan_id', 'id');
    }
}
