<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class roleuser extends Model
{
    protected $table = 'role_users';

	const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

     protected $casts = [
        'user_id' => 'integer',
        'role_id' => 'integer',
    ];
}
