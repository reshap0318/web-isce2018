<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class proposal extends Model
{
    protected $table = 'proposals';

	const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

     protected $casts = [
        'kelompok_id' => 'integer',
        'nama' => 'string',
        'user_id' => 'integer',
    ];

    protected $fillable = [
        'nama',
        'kelompok_id',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(user::class, 'user_id', 'id');
    }

    public function kelompok()
    {
        return $this->belongsTo(kelompok::class, 'kelompok_id', 'id');
    }
}
