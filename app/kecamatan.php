<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kecamatan extends Model
{
    protected $table = 'kecamatans';

	const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

     protected $casts = [
        'nama_kec' => 'string',
        'kab_kota_id' => 'integer',
    ];

    protected $fillable = [
        'nama_kec',
        'kab_kota_id',
    ];

    public function kabkot()
    {
        return $this->belongsTo(kab_kota::class, 'kab_kota_id', 'id');
    }
}
