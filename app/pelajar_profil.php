<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pelajar_profil extends Model
{
    protected $table = 'pelajar_profils';

	const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

     protected $casts = [
        'id' => 'integer',
        'nisn' => 'string',
        'sekolah_id' => 'integer',
        'bukti_pembayaran' => 'string',
    ];

    protected $fillable = [
        'id',
        'nis',
        'sekolah_id',
        'bukti_pembayaran',
    ];

    public function user()
    {
        return $this->belongsTo(user::class, 'id', 'id');
    }

    public function sekolah()
    {
        return $this->belongsTo(sekolah::class, 'sekolah_id', 'id');
    }
}
