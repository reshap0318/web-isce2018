<?php

namespace App\Http\Controllers;

use App\mahasiswa_profil;
use Illuminate\Http\Request;
use App\universitas;
use Sentinel;

class MahasiswaProfilController extends Controller
{
    public function index()
    {
        $mahasiswas = mahasiswa_profil::all();
        return view('mahasiswa.index',compact('mahasiswas'));
    }

    public function create()
    {
        $univ = universitas::pluck('nama','id');
        return view('mahasiswa.create',compact('univ'));
    }

   
    public function store(Request $request)
    {
        $request->validate([
            'nim' => 'required|unique:mahasiswa_profils',
            'universitas_id' => 'required',
            'jurusan' => 'required',
        ]);

        $mahasiswa = new mahasiswa_profil;
        $mahasiswa->id = Sentinel::getuser()->id;
        $mahasiswa->nim = $request->nim;
        $mahasiswa->universitas_id = $request->universitas_id;
        $mahasiswa->jurusan = $request->jurusan;
        $mahasiswa->ktm = $request->ktm;
        $mahasiswa->bukti_pembayaran = $request->bukti_pembayaran;
        if($mahasiswa->save()){
            return redirect()->route('mahasiswa.index')->with(['success'=>'coba dulu']);
        }else{
            return redirect()->back();
        }
    }

    public function show(mahasiswa_profil $mahasiswa_profil)
    {
        dd('belum di buat');
    }


    public function edit($mahasiswa_profil)
    {
        $univ = universitas::pluck('nama','id');
        $mahasiswa_profil = mahasiswa_profil::find($mahasiswa_profil);
        //dd([$univ,$mahasiswa_profil]);
        return view('mahasiswa.edit',compact('univ','mahasiswa_profil'));
    }

    public function update(Request $request,$mahasiswa_profil)
    {
        $request->validate([
            'nim' => 'required',
            'universitas_id' => 'required',
            'jurusan' => 'required',
        ]);

        $mahasiswa_profil = mahasiswa_profil::find($mahasiswa_profil);
        $mahasiswa = $mahasiswa_profil;
        $mahasiswa->id = Sentinel::getuser()->id;
        $mahasiswa->nim = $request->nim;
        $mahasiswa->universitas_id = $request->universitas_id;
        $mahasiswa->jurusan = $request->jurusan;
        $mahasiswa->ktm = $request->ktm;
        $mahasiswa->bukti_pembayaran = $request->bukti_pembayaran;
        if($mahasiswa->update()){
            return redirect()->route('mahasiswa.index');
        }else{
            return redirect()->back();
        }
    }

    public function destroy($mahasiswa_profil)
    {

        $mahasiswa_profil = mahasiswa_profil::find($mahasiswa_profil);
        if($mahasiswa_profil->delete()){
            return redirect()->route('mahasiswa.index');
        }else{
            return redirect()->back();
        }
    }
}
