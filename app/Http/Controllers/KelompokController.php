<?php

namespace App\Http\Controllers;

use App\kelompok;
use App\user;
use Illuminate\Http\Request;

class KelompokController extends Controller
{
    
    public function index()
    {
        $kelompoks = kelompok::all();
        return view('kelompok.index',compact('kelompoks'));
    }

    
    public function create()
    {
        $user = user::pluck('nama','id');
        return view('kelompok.create',compact('user'));
    }

    
    public function store(Request $request)
    {
        $request->validate([
            'ketua_id' => 'required',
            'nama' => 'required|unique:kelompoks',
        ]);
        $kelompok = new kelompok;
        $kelompok->ketua_id = $request->ketua_id;
        $kelompok->nama = $request->nama;
        if($kelompok->save()){
            return redirect()->route('kelompok.index');
        }else{
            return redirect()->back();
        }
    }

    
    public function show(kelompok $kelompok)
    {
        dd('belum');
    }

   
    public function edit(kelompok $kelompok)
    {
        $user = user::pluck('nama','id');
        return view('kelompok.edit',compact('user','kelompok'));
    }

    
    public function update(Request $request, kelompok $kelompok)
    {
        $request->validate([
            'ketua_id' => 'required',
            'nama' => 'required',
        ]);
        $kelompok->ketua_id = $request->ketua_id;
        $kelompok->nama = $request->nama;
        if($kelompok->update()){
            return redirect()->route('kelompok.index');
        }else{
            return redirect()->back();
        }
    }

    
    public function destroy(kelompok $kelompok)
    {
        if($kelompok->delete()){
            return redirect()->route('kelompok.index');
        }else{
            return redirect()->back();
        }
    }
}
