<?php

namespace App\Http\Controllers;

use App\universitas;
use Illuminate\Http\Request;
use App\kecamatan;
use App\provinsi;
use Sentinel;

class UniversitasController extends Controller
{
    public function index()
    {
        $universitass = universitas::all();
        return view('universitas.index',compact('universitass'));
    }

    public function create()
    {
        # Ambil semua isi tabel tujuan dari model
        $provinsi = provinsi::all();
        # Inisialisasi variabel daftar dengan array
        $daftar = array('' => '');
        # lakukan perulangan untuk provinsi
        foreach($provinsi as $temp)
            # Isi daftar dengan nama (provinsi) berdasarkan id
            $daftar[$temp->id] = $temp->nama_prov;
        # Tampilkan halaman index beserta variabel daftar
        $kecamatan = kecamatan::pluck('nama_kec','id');
        return view('universitas.create',compact('kecamatan','daftar'));
    }

    public function postDropdown() 
    {   
        # Tarik ID inputan
        $set = Input::get('id');
        dd($set);

        # Inisialisasi variabel berdasarkan masing-masing tabel dari model
        # dimana ID target sama dengan ID inputan
        $kabupaten = Kabupaten::where('id_provinsi', $set)->get();
        $kecamatan = Kecamatan::where('id_kabupaten_kota', $set)->get();
        $kelurahan = Kelurahan::where('id_kecamatan', $set)->get();

        # Buat pilihan "Switch Case" berdasarkan variabel "type" dari form
        switch(Input::get('type')):
            # untuk kasus "kabupaten"
            case 'kabupaten':
                # buat nilai default
                $return = '<option value="">Pilih Kabupaten...</option>';
                # lakukan perulangan untuk tabel kabupaten lalu kirim
                foreach($kabupaten as $temp) 
                    # isi nilai return
                    $return .= "<option value='$temp->id'>$temp->nama</option>";
                # kirim
                return $return;
            break;
            # untuk kasus "kecamatan"
            case 'kecamatan':
                $return = '<option value="">Pilih Kecamatan...</option>';
                foreach($kecamatan as $temp) 
                    $return .= "<option value='$temp->id'>$temp->nama</option>";
                return $return;
            break;
            # untuk kasus "kelurahan"
            case 'kelurahan':
                $return = '<option value="">Pilih Kelurahan...</option>';
                foreach($kelurahan as $temp) 
                    $return .= "<option value='$temp->id'>$temp->nama</option>";
                return $return;
            break;
        # pilihan berakhir
        endswitch;    
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:universitas',
            //'kecamatan_id' => 'required',
            'alamat' => 'required',
        ]);
        $universitas = new universitas;
        $universitas->nama = $request->nama;
        $universitas->kode_pos = $request->kode_pos;
       // $universitas->kecamatan_id = $request->kecamatan_id;
        $universitas->alamat = $request->alamat;
        if($universitas->save()){
            return redirect()->route('universitas.index');
        }else{
            return redirect()->back();
        }
    }

    public function show(universitas $universitas)
    {
        return view('universitas.show',compact('universitas'));
    }

    public function edit($id)
    {
        $universitas = universitas::find($id);
        $kecamatan = kecamatan::pluck('nama_kec','id');
        return view('universitas.edit',compact('kecamatan','universitas'));
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'nama'          => 'required',
            //'kecamatan_id'  => 'required',
            'alamat'        => 'required',
        ]);

        $universitas = universitas::find($id);
        $universitas->nama = $request->nama;
        $universitas->kode_pos = $request->kode_pos;
        //$universitas->kecamatan_id = $request->kecamatan_id;
        $universitas->alamat = $request->alamat;
        if($universitas->save()){
            return redirect()->route('universitas.index');
        }else{
            return redirect()->back();
        }
    }

    public function destroy($id)
    {
        $universitas = universitas::find($id);
        if($universitas->delete()){
            return redirect()->route('universitas.index');
        }else{
            return redirect()->back();
        }
    }

    public function universitas()
    {
        return view('uiuser.universitas');
    }

    public function simpanuni(Request $request)
    {
        $request->validate([
            'nama'          => 'required',
            'kode_pos'          => 'required',
            'alamat'        => 'required',
        ]);

        $universitas = new universitas;
        $universitas->nama = $request->nama;
        $universitas->kode_pos = $request->kode_pos;
        $universitas->alamat = $request->alamat;
        if($universitas->save()){
            toast()->success('Berhasil menambahkan '.$universitas->nama);
            return redirect('editprofil/'.Sentinel::getuser()->nama.'/'.Sentinel::getuser()->id);
        }else{
            toast()->error('Gagal Menambahkan '.$universitas->nama);
            return redirect()->back();
        }
    }
}
