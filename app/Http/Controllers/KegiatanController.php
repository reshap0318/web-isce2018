<?php

namespace App\Http\Controllers;

use App\kegiatan;
use Illuminate\Http\Request;
use App\faq;

class KegiatanController extends Controller
{
    public function index()
    {
        $kegiatans = kegiatan::all();
        return view('kegiatan.index',compact('kegiatans'));
    }
    public function create()
    {
        return view('kegiatan.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'nama'              => 'required|unique:kegiatans',
            'tema'              => 'required|unique:kegiatans',
            'waktu_mulai'       => 'required',
            'waktu_berakhir'    => 'required',
            'lokasi'            => 'required',
            'latar_belakang'    => 'required',
            'tujuan'            => 'required',
            'deskripsi'         => 'required',
        ]);

        $kegiatan = new kegiatan;
        $kegiatan->nama = $request->nama;
        $kegiatan->tema = $request->tema;
        $kegiatan->waktu_mulai = $request->waktu_mulai;
        $kegiatan->waktu_berakhir = $request->waktu_berakhir;
        $kegiatan->insert = $request->insert;
        $kegiatan->lokasi = $request->lokasi;
        $kegiatan->latar_belakang = $request->latar_belakang;
        $kegiatan->tujuan = $request->tujuan;
        $kegiatan->deskripsi = $request->deskripsi;
        if($kegiatan->save()){
            return redirect()->route('kegiatan.index');
        }else{
            return redirect()->back();
        }
    }
    public function show(kegiatan $kegiatan)
    {
        dd('belum');
    }
    public function edit(kegiatan $kegiatan)
    {
        return view('kegiatan.edit',compact('kegiatan'));
    }
    public function update(Request $request, kegiatan $kegiatan)
    {
        $request->validate([
            'nama'              => 'required',
            'tema'              => 'required',
            'waktu_mulai'       => 'required',
            'waktu_berakhir'    => 'required',
            'lokasi'            => 'required',
            'latar_belakang'    => 'required',
            'tujuan'            => 'required',
            'deskripsi'         => 'required',
        ]);
        $kegiatan->nama = $request->nama;
        $kegiatan->tema = $request->tema;
        $kegiatan->waktu_mulai = $request->waktu_mulai;
        $kegiatan->waktu_berakhir = $request->waktu_berakhir;
        $kegiatan->insert = $request->insert;
        $kegiatan->lokasi = $request->lokasi;
        $kegiatan->latar_belakang = $request->latar_belakang;
        $kegiatan->tujuan = $request->tujuan;
        $kegiatan->deskripsi = $request->deskripsi;
        if($kegiatan->update()){
            return redirect()->route('kegiatan.index');
        }else{
            return redirect()->back();
        }
    }
    public function destroy(kegiatan $kegiatan)
    {
        if($kegiatan->delete()){
            return redirect()->route('kegiatan.index');
        }else{
            return redirect()->back();
        }
    }

    public function olkom()
    {
        return view('lomba.olkom');
    }

    public function hack()
    {
        return view('lomba.hackathon');
    }

    public function faq()
    {
        return view('lomba.faq');
    }

    public function faqt(Request $request)
    {
       $request->validate([
            'nama' => 'required',
            'email' => 'required',
            'judul' => 'required',
            'pesan' => 'required',
        ]);
       
        $faq = new faq;
        $faq->nama = $request->nama;
        $faq->email = $request->email;
        $faq->judul = $request->judul;
        $faq->pesan = $request->pesan;
        if($faq->Save()){
            toast()->success('Terimakasih Telah Membantu Kami');
        }
            return redirect()->back();
    }
}
