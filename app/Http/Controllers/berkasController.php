<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\user_lomba as peserta;
use App\mahasiswa_profil as mahasiswa;
use App\pelajar_profil as pelajar;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Sentinel;
use App\user;
use App\kelompok;
use App\proposal;

class berkasController extends Controller
{
    

    public function index($user, $id)
    {

        //umum
        $pembayaran = "x";
        $foto = "x";
        //mahasiswa
        $ktm = "x";
        $nim = "x";
        $universitas = "x";
        $jurusan = "x";
        //pelajar
        $nisn = "x";
        $sekolah = "x";

        $status = "mahasiswa";
        $pes = "x";
        $ketua = "x";
        
    	//dd([$user,$id]);
    	$peserta = peserta::where('user_id',$id)->first();
        $user = Sentinel::getuser();

        if($user->bukti_pembayaran){
            $pembayaran = "v";
        }

        if($user->foto){
            $foto = "v";
        }

        if($user->pelajar){
            if($user->pelajar->nisn){
                $nisn = "v";
            }

            if($user->pelajar->sekolah_id){
                $nisn = "v";
            }
            $status = "pelajar";
            $nim = "v";
            $ktm = "v";
            $universitas = "v";
            $jurusan = "v";
            $ketua = "v"; 
        }elseif($user->mahasiswa){
            if($user->mahasiswa->nim){
                $nim = "v";
            }

            if($user->mahasiswa->ktm){
                $ktm = "v";
            }

            if($user->mahasiswa->universitas_id){
                $universitas = "v";
            }

            if($user->mahasiswa->jurusan){
                $jurusan = "v";
            }

            if($user->kelompok){
               $ketua = "v"; 
            }
            $nisn = "v";
            $sekolah = "v";
        }

    	if($peserta){
    		$pes = "v";
    	}
    	//dd([$user->bukti_pembayaran, $user->foto, $user->pelajar, $user->pelajar, $user->mahasiswa->nim, $user->mahasiswa->ktm, $user->mahasiswa->universitas_id, $user->mahasiswa->jurusan,$peserta]);
    	return view('uiuser.index',compact('pembayaran','foto','ktm','nim', 'universitas', 'jurusan', 'nisn','sekolah','status','peserta','ketua'));
    }

    public function file($type, $file_id)
    {
        $lokasi = null;
        
        if ($type == 'ktm') {
            $lokasi = 'img/ktm';
        }elseif ($type == 'foto') {
            $lokasi = 'img/foto';
        }elseif ($type == 'bayar') {
            $lokasi = 'img/bayar';
        }elseif ($type == 'proposal') {
            $lokasi = 'proposal';
        }elseif ($type == 'panduan') {
            $lokasi = 'panduan';
        }elseif ($type == 'pengumuman') {
            $lokasi = 'pengumuman';
        }
        return response()->file(
            storage_path('app/'.$lokasi.'/'.$file_id)
        );
    }

    public function indexhack($user, $id)
    {
    	//0. cari peserta
        //1. cek, apakah dia ini mahasiswa atau tidak
        // -ya, lanjut ke no2
        // -tidak, menu hackathon dan menuju hackathon di larang
        //2. cek kelompok, apakah dia seorang ketua, 
        // -iya, bisa menambahkan anggota, tidak bisa membuat kelompok
        // -tidak, bisa membuat kelompok, dan menjadi ketua
        //3. cek jumlah kelompok, max 3,
        // -=3, tidak bisa menambahkan anggota lagi, form proposal open
        // -<3, bisa menambahkan anggota, form proposal close
        //4. cek proposal, apakah sudah ada yang di upload oleh kelompok ini atau belum,
        // -sudah, tidak bisa menambkan dan mengedit anggota lagi, dan cek, apalah proposal telah di upload sebanyak 3x
            // -=3, tidak bisa menambahkan proposal lagi,
            // -<3, bisa menambahkan proposal lagi
        // -belum, bisa menambahkan dan mengedit anggota

        //umum
        $pembayaran = "x";
        $foto = "x";
        //mahasiswa
        $ktm = "x";
        $nim = "x";
        $universitas = "x";
        $jurusan = "x";
        //pelajar
        $nisn = "x";
        $sekolah = "x";

        $status = "mahasiswa";
        $pes = "x";
        $ketua = "x";
        
        //dd([$user,$id]);
        $peserta = peserta::where('user_id',$id)->first();
        $user = Sentinel::getuser();

        if(!$user->bukti_pembayaran){
            toast()->error('Tidak Bisa Mengakses, Anda Harus Melakukan Pembayaran Terlebih Dahulu');
            return redirect()->back();
        }elseif(!$user->mahasiswa->ktm){
            toast()->error('Tidak Bisa Mengakses, Anda Harus Mengupload KTM Terlebih Dahulu');
            return redirect()->back();
        }elseif(!$user->mahasiswa->nim || !$user->mahasiswa->universitas_id || !$user->mahasiswa->jurusan){
            toast()->error('Tidak Bisa Mengakses, Anda Harus Melengkapi Data Diri Terlebih Dahulu');
            return redirect()->back();
        }

        if($user->foto){
            $foto = "v";
        }

        if($user->pelajar){
            if($user->pelajar->nisn){
                $nisn = "v";
            }

            if($user->pelajar->sekolah_id){
                $nisn = "v";
            }
            $status = "pelajar";
            $nim = "v";
            $ktm = "v";
            $universitas = "v";
            $jurusan = "v";
            $ketua = "v"; 
        }elseif($user->mahasiswa){
            if($user->mahasiswa->nim){
                $nim = "v";
            }

            if($user->mahasiswa->ktm){
                $ktm = "v";
            }

            if($user->mahasiswa->universitas_id){
                $universitas = "v";
            }

            if($user->mahasiswa->jurusan){
                $jurusan = "v";
            }

            if($user->kelompok){
               $ketua = "v"; 
            }
            $nisn = "v";
            $sekolah = "v";
        }

        if($peserta){
            $pes = "v";
        }
        //dd([$user->bukti_pembayaran, $user->foto, $user->pelajar, $user->pelajar, $user->mahasiswa->nim, $user->mahasiswa->ktm, $user->mahasiswa->universitas_id, $user->mahasiswa->jurusan,$peserta]);

        //load proposal berdasarkan kelompok pesertta
        $proposal = [];
        if($peserta){
            $proposal = proposal::where('kelompok_id',$peserta->kelompok_id)->get();
            $kelompok = peserta::where('kelompok_id',$peserta->kelompok_id)->get();
        }
        $jumlah_proposal = proposal::groupBy('kelompok_id')->count();
        $jumlah_anggota = peserta::groupBy('kelompok_id')->count();
        
        //dd($jumlah_proposal);
    	return view('uiuser.hackathon',compact('pembayaran','foto','ktm','nim', 'universitas', 'jurusan', 'nisn','sekolah','status','peserta','ketua','pes','kelompok', 'jumlah_proposal', 'jumlah_anggota','proposal'));
    }

    public function ktm(Request $request, $id)
    {

        $request->validate([
            'ktm' => 'image|mimes:jpg,png,jpeg,gif',
        ]);
        $mahasiswa = mahasiswa::find($id);
        $path = 'img/ktm';
        //dd([$id,$mahasiswa]);
        $oldfile = $mahasiswa->ktm;
        if ($request->hasFile('ktm') && $request->ktm->isValid()) {
            $fileext = $request->ktm->extension();
            
            $filename = $request->file('ktm')->getClientOriginalName();
            //dd([$fileext,$filename]);
            //Real File
            $filepath = $request->file('ktm')->storeAs($path, $filename, 'local');
            //ktm File
            $realpath = storage_path('app/'.$filepath);
            $mahasiswa->ktm = $filename;
            //hapus foto lama
            File::delete(storage_path('app'.'/'. $path . '/' . $oldfile));
            File::delete(public_path($path . '/' . $oldfile));
        }

        if($mahasiswa->update()){
            return redirect()->route('berkas.index',[Sentinel::getuser()->nama, Sentinel::getuser()->id]);
        }else{
            return redirect()->back();
        }
    }

    public function foto(Request $request, $id)
    {
        $request->validate([
            'foto' => 'image|mimes:jpg,png,jpeg,gif',
        ]);
        $mahasiswa = Sentinel::getuser();
        //dd($mahasiswa);
        $path = 'img/foto';
        $oldfile = $mahasiswa->foto;
        if ($request->hasFile('foto') && $request->foto->isValid()) {
            $fileext = $request->foto->extension();
            
            $filename = $request->file('foto')->getClientOriginalName();
            //dd([$fileext,$filename]);
            //Real File
            $filepath = $request->file('foto')->storeAs($path, $filename, 'local');
            //foto File
            $realpath = storage_path('app/'.$filepath);
            $mahasiswa->foto = $filename;
            //hapus foto lama
            File::delete(storage_path('app'.'/'. $path . '/' . $oldfile));
            File::delete(public_path($path . '/' . $oldfile));
        }

        if($mahasiswa->update()){
            return redirect('profil/'.Sentinel::getuser()->nama.'/'.Sentinel::getuser()->id);
        }else{
            return redirect()->back();
        }
    }

    public function bayar(Request $request, $id)
    {
        $request->validate([
            'bayar' => 'image|mimes:jpg,png,jpeg,gif',
        ]);
        $user = user::find($id);
        //dd($user);
        $path = 'img/bayar';
        $oldfile = $user->bukti_pembayaran;
        if ($request->hasFile('bayar') && $request->bayar->isValid()) {
            $fileext = $request->bayar->extension();
            
            $filename = $request->file('bayar')->getClientOriginalName();
            //dd([$fileext,$filename]);
            //Real File
            $filepath = $request->file('bayar')->storeAs($path, $filename, 'local');
            //bayar File
            $realpath = storage_path('app/'.$filepath);
            $user->bukti_pembayaran = $filename;
            //hapus bayar lama
            File::delete(storage_path('app'.'/'. $path . '/' . $oldfile));
            File::delete(public_path($path . '/' . $oldfile));
        }

        if($user->update()){
            return redirect()->route('berkas.index',[Sentinel::getuser()->nama, Sentinel::getuser()->id]);
        }else{
            return redirect()->back();
        }
    }

    public function makegrup(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:kelompoks',
        ]);
        $kelompok = new kelompok;
        $kelompok->ketua_id = Sentinel::getuser()->id;
        $kelompok->nama = $request->nama;
        if($kelompok->save()){
            $peserta = new peserta;
            $peserta->user_id = Sentinel::getuser()->id;
            $peserta->kelompok_id = $kelompok->id;
            $peserta->kegiatan_id = '1';
            if($peserta->save()){
                return redirect()->route('hackathon.index',[Sentinel::getuser()->nama, Sentinel::getuser()->id]);
            }else{
                return redirect()->back();
            }
        }else{
            return redirect()->back();
        }
    }

    public function addanggota(Request $request)
    {
        //cari user
        
        $user = user::where('username',$request->username)->first();
        
        if($user){

            if(!$user->bukti_pembayaran){
                toast()->error('User yang anda cari Belum Melakukan Pembayaran');
                return redirect()->back();
            }

            if(!$user->foto){
                toast()->error('User yang anda cari Belum Melengkapi Data Dirinya');
                return redirect()->back();
            }

            if($user->mahasiswa){
                if($user->mahasiswa->nim=="xxxxxxxxxx" || !$user->mahasiswa->universitas_id || !$user->mahasiswa->jurusan || !$user->mahasiswa->ktm){
                    toast()->error('User yang anda cari Belum Melengkapi Data Dirinya');
                    return redirect()->back();
                }
            }else{
                toast()->error('User yang anda cari Bukan Mahasiswa');
                return redirect()->back();
            }

            $peserta = peserta::where('user_id',$user->id)->first();
            if($peserta){
                toast()->error('User yang anda cari Sudah Memiliki Kelompok');
                return redirect()->back();
            }else{
                $pess = new peserta;
                $pess->user_id = $user->id;
                //dd(Sentinel::getuser()->peserta);
                $pess->kegiatan_id = Sentinel::getuser()->peserta->kegiatan_id;
                $pess->kelompok_id = Sentinel::getuser()->kelompok->id;
                if($pess->save()){
                    //berhasil
                    toast()->success('Berhasil menambahkan '.$user->nama.' sebagai anggota tim');
                }else{
                    toast()->error('Gagal menambahkan '.$user->nama.' sebagai anggota tim');
                }
            }
        }else{
            toast()->error('User yang anda cari tidak ditemukan');
        }

        return redirect()->back();
    }

    public function addproposal(Request $request, $id)
    {
        $request->validate([
            'proposal' => 'file|mimes:pdf,doc,docs,docx',
        ]);
        $proposal = new proposal;

        //dd($mahasiswa);
        $path = 'proposal';
        $proposal->user_id = Sentinel::getuser()->id;
        $proposal->kelompok_id = Sentinel::getuser()->kelompok->id;
        if ($request->hasFile('proposal') && $request->proposal->isValid()) {
            $fileext = $request->proposal->extension();
            
            $filename = $request->file('proposal')->getClientOriginalName();
            //dd([$fileext,$filename]);
            //Real File
            $filepath = $request->file('proposal')->storeAs($path, $filename, 'local');
            //bayar File
            $realpath = storage_path('app/'.$filepath);
            $proposal->nama = $filename;
        }
        //dd([$proposal]);

        if($proposal->save()){
            return redirect()->route('hackathon.index',[Sentinel::getuser()->nama, Sentinel::getuser()->id]);
        }else{
            return redirect()->back();
        }
    }

    public function dellproposal($id)
    {
        $proposal = proposal::find($id);
        $oldfile = $proposal->nama;
        //dd($proposal);
        if($proposal->delete()){
            File::delete(storage_path('app'.'/'. 'proposal' . '/' . $oldfile));
            File::delete(public_path('proposal' . '/' . $oldfile));
            toast()->success('Berhasil Menghapus Proposal');
        }else{
            toast()->error('Gagal Menghapus Proposal');
        }

        return redirect()->back();
    }

    public function break()
    {
        //umum
        $pembayaran = "x";
        $foto = "x";
        //mahasiswa
        $ktm = "x";
        $nim = "x";
        $universitas = "x";
        $jurusan = "x";
        //pelajar
        $nisn = "x";
        $sekolah = "x";

        $status = "mahasiswa";
        $pes = "x";
        $ketua = "x";
        
        //dd([$user,$id]);
        $peserta = peserta::where('user_id',Sentinel::getuser()->id)->first();
        $user = Sentinel::getuser();

        if($user->bukti_pembayaran){
            $pembayaran = "v";
        }

        if($user->foto){
            $foto = "v";
        }

        if($user->pelajar){
            if($user->pelajar->nisn){
                $nisn = "v";
            }

            if($user->pelajar->sekolah_id){
                $nisn = "v";
            }
            $status = "pelajar";
            $nim = "v";
            $ktm = "v";
            $universitas = "v";
            $jurusan = "v";
            $ketua = "v"; 
        }elseif($user->mahasiswa){
            if($user->mahasiswa->nim){
                $nim = "v";
            }

            if($user->mahasiswa->ktm){
                $ktm = "v";
            }

            if($user->mahasiswa->universitas_id){
                $universitas = "v";
            }

            if($user->mahasiswa->jurusan){
                $jurusan = "v";
            }

            if($user->kelompok){
               $ketua = "v"; 
            }
            $nisn = "v";
            $sekolah = "v";
        }

        if($peserta){
            $pes = "v";
        }

        
        //dd([$user->bukti_pembayaran, $user->foto, $user->pelajar, $user->pelajar, $user->mahasiswa->nim, $user->mahasiswa->ktm, $user->mahasiswa->universitas_id, $user->mahasiswa->jurusan,$peserta]);
        return view('dashboard',compact('pembayaran','foto','ktm','nim', 'universitas', 'jurusan', 'nisn','sekolah','status','peserta','ketua','pes'));
    }

    
}
