<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\user;
use App\mahasiswa_profil as mahasiswa;
use App\pelajar_profil as pelajar;
use App\universitas;
use App\sekolah;
use Sentinel;
use Activation;

class userController extends Controller
{
 	public function index()
 	{
 		$users = user::all();
 		return view('user.index',compact('users'));
 	}

 	public function create()
 	{
 		return view('user.create');
 	}

 	public function store(Request $request)
    {
        $request->validate([
        	'username' => 'required|unique:users',
            'nama' => 'required',
            'email' => 'required',
            'no_hp' => 'required',
            'password' => 'required|same:password_confirm',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            //'jenis_kelamin' => 'required',
            'kecamatan_id' => 'required',
        ]);
        //dd($request->request);
        $user = new user;
        $user->username = $request->username;
        $user->nama = $request->nama;
        $user->email = $request->email;
        $user->no_hp = $request->no_hp;
        $user->password = bcrypt($request->password);
        $user->tempat_lahir = $request->tempat_lahir;
        $user->tanggal_lahir = $request->tanggal_lahir;
        $user->jenis_kelamin = $request->jenis_kelamin;
        $user->kecamatan_id = $request->kecamatan_id;
        $user->alamat = $request->alamat;
        if($user->save()){
            $activation = Activation::create($user);
            $activation = Activation::complete($user, $activation->code);
        	return redirect()->route('pemilik-akun.index');
        }else{
        	return redirect()->back();
        }
    }

    
    public function show($id)
    {
        $user = user::find($id);
        return view('user.show');
    }

    
    public function edit($id)
    {
        $user = user::find($id);
        return view('user.edit',compact('user'));
    }

    
    public function update(Request $request, $id)
    {
        $request->validate([
            'username' => 'required',
            'nama' => 'required',
            'email' => 'required',
            'no_hp' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'kecamatan_id' => 'required',
        ]);

        $user = user::find($id);
        $user->username = $request->username;
        $user->nama = $request->nama;
        $user->email = $request->email;
        $user->no_hp = $request->no_hp;
        $user->password = bcrypt($request->password);
        $user->tempat_lahir = $request->tempat_lahir;
        $user->tanggal_lahir = $request->tanggal_lahir;
        $user->jenis_kelamin = $request->jenis_kelamin;
        $user->kecamatan_id = $request->kecamatan_id;
        $user->alamat = $request->alamat;
        if($user->save()){
            
            return redirect()->route('pemilik-akun.index');
        }else{
            return redirect()->back();
        }
    }

    
    public function destroy($id)
    {
        $user = user::find($id);
        //dd([$user,$id]);
        if($user->delete()){
        	return redirect()->route('pemilik-akun.index');
        }else{
        	return redirect()->back();
        }
    }

    public function password()
    {
        return view('uiuser.gantipassword');
    }

    public function ganti(Request $request)
    {
        $request->validate([
            'password' => 'required|same:password_confirm|min:6',
        ]);
        $user = user::find(Sentinel::getuser()->id);
        $user->password = bcrypt($request->password);
        if($user->update()){
            toast()->success('Berhasil Mengganti Password');
        }else{
            toast()->error('Gagal Mengganti Password');
        }

        return redirect()->back();
    }

    public function profil($nama, $id)
    {
        $user = user::find($id);
        return view('uiuser.profil',compact('user'));
    }

    public function editprofil()
    {
        $universitas = universitas::select('id','nama')->get();
        $sekolah = sekolah::select('id','nama')->get();
        return view('uiuser.editprofil',compact('universitas','sekolah'));
    }

    public function simpanprofil(Request $request)
    {
        //dd($request->request);
        $id = Sentinel::getuser()->id;
        $user = user::find($id);
        $mahasiswa = mahasiswa::find($id);
        $pelajar = pelajar::find($id);

        if($request->nama){
            $user->nama = $request->nama;
        }

        if($request->nim){
            $mahasiswa->nim = $request->nim;
        }elseif($request->nisn){
            $pelajar->nisn = $request->nisn;
        }

        if($request->email){
            $user->email = $request->email;
        }

        if($request->no_hp){
            $user->no_hp = $request->no_hp;
        }

        if($request->email){
            $user->email = $request->email;
        }

        if($request->universitas){
            $mahasiswa->universitas_id = $request->universitas;
        }elseif($request->sekolah){
            $pelajar->sekolah_id = $request->sekolah;
        }

        if($request->jurusan){
            $mahasiswa->jurusan = $request->jurusan;
        }

        if($request->jenis_kelamin){
            $user->jenis_kelamin = $request->jenis_kelamin;
        }

        if($request->tempat_lahir){
            $user->tempat_lahir = $request->tempat_lahir;
        }

        if($request->tanggal_lahir){
            $user->tanggal_lahir = $request->tanggal_lahir;
        }

        if($request->alamat){
            $user->alamat = $request->alamat;
        }
        //dd([$request->request, $user, $mahasiswa, $pelajar]);
        if($user->update()){
            if($mahasiswa){
                //dd($mahasiswa);
                $mahasiswa->update();
            }elseif($pelajar){
                //dd($request->mahasiswa);
                $pelajar->update();
            }else{
                //dd([$request->mahasiswa, $request->pelajar]);
                return redirect()->back();
            }
            return redirect('profil/'.Sentinel::getuser()->nama.'/'.Sentinel::getuser()->id);
        }

        return redirect()->back();
    }

    public function bukti()
    {
        return view('uiuser.bukti');
    }
}
