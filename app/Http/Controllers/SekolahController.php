<?php

namespace App\Http\Controllers;

use App\sekolah;
use App\kecamatan;
use Illuminate\Http\Request;
use Sentinel;

class SekolahController extends Controller
{
    public function index() 
    {
        $sekolahs = sekolah::all();
        return view('sekolah.index',compact('sekolahs'));
    }

    public function create()
    {
        $kecamatan = kecamatan::pluck('nama_kec','id');
        return view('sekolah.create',compact('kecamatan'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama'          => 'required|unique:sekolahs',
            //'kecamatan_id'  => 'required',
            'alamat'        => 'required',
        ]);

        $sekolah = new sekolah;
        $sekolah->nama = $request->nama;
        $sekolah->kontak = $request->kontak;
        $sekolah->nama_kontak = $request->nama_kontak;
        $sekolah->kode_pos = $request->kode_pos;
        $sekolah->alamat = $request->alamat;
        //$sekolah->kecamatan_id = $request->kecamatan_id;
        if($sekolah->save()){
            return redirect()->route('sekolah.index');
        }else{
            return redirect()->back();
        }
    }

    public function show(sekolah $sekolah)
    {
        return view('sekolah.show',compact('sekolah'));
    }

    public function edit(sekolah $sekolah)
    {
        $kecamatan = kecamatan::pluck('nama_kec','id');
        return view('sekolah.edit',compact('kecamatan','sekolah'));
    }

    public function update(Request $request, sekolah $sekolah)
    {
        $request->validate([
            'nama'          => 'required',
            //'kecamatan_id'  => 'required',
            'alamat'        => 'required',
        ]);
        $sekolah->nama = $request->nama;
        $sekolah->kontak = $request->kontak;
        $sekolah->nama_kontak = $request->nama_kontak;
        $sekolah->kode_pos = $request->kode_pos;
        $sekolah->alamat = $request->alamat;
        //$sekolah->kecamatan_id = $request->kecamatan_id;
        if($sekolah->save()){
            return redirect()->route('sekolah.index');
        }else{
            return redirect()->back();
        }
    }

    public function destroy(sekolah $sekolah)
    {
        if($sekolah->delete()){
            return redirect()->route('sekolah.index');
        }else{
            return redirect()->back();
        }
    }

    public function sekolah()
    {
        return view('uiuser.sekolah');
    }

    public function simpanseko(Request $request)
    {
        //dd($request->request);
        $request->validate([
            'nama'          => 'required',
            'alamat'        => 'required',
        ]);
        $sekolah = new Sekolah;
        $sekolah->nama = $request->nama;
        $sekolah->kontak = $request->kontak;
        $sekolah->nama_kontak = $request->nama_kontak;
        $sekolah->kode_pos = $request->kode_pos;
        $sekolah->alamat = $request->alamat;
        //$sekolah->kecamatan_id = $request->kecamatan_id;
        if($sekolah->save()){
            return redirect('editprofil/'.Sentinel::getuser()->nama.'/'.Sentinel::getuser()->id);
        }else{ 
            return redirect()->back();
        }

    }
}
