<?php

namespace App\Http\Controllers;

use App\kecamatan;
use App\kab_kota;
use Illuminate\Http\Request;

class KecamatanController extends Controller
{
     
    public function index()
    {
        $kecamatans = kecamatan::all();
        return view('kecamatan.index',compact('kecamatans'));
    }

    
    public function create()
    {
        $kab_kota = kab_kota::pluck('nama_kab_kota','id');
        return view('kecamatan.create',compact('kab_kota'));
    }

    
    public function store(Request $request)
    {
        $request->validate([
            'nama_kec' => 'required|unique:kecamatans',
            'kab_kota_id' => 'required',
        ]);
        $kecamatan = new kecamatan;
        $kecamatan->nama_kec = $request->nama_kec;
        $kecamatan->kab_kota_id = $request->kab_kota_id;
        if($kecamatan->save()){
            return redirect()->route('kecamatan.index');
        }else{
            return redirect()->back();
        }
    }

    
    public function show(kecamatan $kecamatan)
    {
        return redirect()->back();
    }

    
    public function edit(kecamatan $kecamatan)
    {
        //dd($kecamatan);
        $kab_kota = kab_kota::pluck('nama_kab_kota','id');
        return view('kecamatan.edit',compact('kab_kota','kecamatan'));
    }

    
    public function update(Request $request, kecamatan $kecamatan)
    {
        $request->validate([
            'nama_kec' => 'required',
            'kab_kota_id' => 'required',
        ]);
        $kecamatan->nama_kec = $request->nama_kec;
        $kecamatan->kab_kota_id = $request->kab_kota_id;
        if($kecamatan->save()){
            return redirect()->route('kecamatan.index');
        }else{
            return redirect()->back();
        }
    }

    
    public function destroy(kecamatan $kecamatan)
    {
        if($kecamatan->delete()){
            return redirect()->route('kecamatan.index');
        }else{
            return redirect()->back();
        }
    }
}
