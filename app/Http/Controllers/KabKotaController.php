<?php

namespace App\Http\Controllers;

use App\kab_kota;
use App\provinsi;
use Illuminate\Http\Request;

class KabKotaController extends Controller
{ 
    
    public function index()
    {
        $kab_kotas = kab_kota::all();
        //dd($kab_kotas);
        return view('kab_kota.index',compact('kab_kotas'));
    }

    
    public function create()
    {
        $prov = provinsi::pluck('nama_prov','id');
        return view('kab_kota.create',compact('prov'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_kab_kota' => 'required|unique:kab_kotas',
            'prov_id' => 'required',
        ]);
        $kab_kota = new kab_kota;
        $kab_kota->nama_kab_kota = $request->nama_kab_kota;
        $kab_kota->prov_id = $request->prov_id;
        if($kab_kota->save()){
            return redirect()->route('kabupaten-kota.index');
        }else{
            return redirect()->back();
        }
    }

    
    public function show(kab_kota $kab_kota)
    {
        return redirect()->back();
    }

    public function edit($kab_kota)
    {
        $kab_kota = kab_kota::find($kab_kota);
        
        $prov = provinsi::pluck('nama_prov','id');
        return view('kab_kota.edit',compact('prov','kab_kota'));
    }

    public function update(Request $request, $kab_kota)
    {
        $request->validate([
            'nama_kab_kota' => 'required',
            'prov_id' => 'required',
        ]);
        $kab_kota = kab_kota::find($kab_kota);
        $kab_kota->nama_kab_kota = $request->nama_kab_kota;
        $kab_kota->prov_id = $request->prov_id;
        if($kab_kota->save()){
            return redirect()->route('kabupaten-kota.index');
        }else{
            return redirect()->back();
        }
    }


    public function destroy($kab_kota)
    {
        $kab_kota = kab_kota::find($kab_kota);
        if($kab_kota->delete()){
            return redirect()->route('kabupaten-kota.index');
        }else{
            return redirect()->back();
        }
    }
}
