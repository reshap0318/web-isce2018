<?php

namespace App\Http\Controllers;

use App\provinsi;
use Illuminate\Http\Request;

class ProvinsiController extends Controller
{
    
    public function index()
    {
        $provinsis = provinsi::all();
        return view('provinsi.index',compact('provinsis'));
    }
 
    
    public function create()
    {
        return view('provinsi.create');
    }

    
    public function store(Request $request)
    {
        $request->validate([
            'nama_prov' => 'required|unique:provinsis'
        ]);

        $provinsi = new provinsi;
        $provinsi->nama_prov = $request->nama_prov;
        if($provinsi->save()){
            return redirect()->route('provinsi.index');
        }else{
            return redirect()->back();
        }
    }

    
    public function show(provinsi $provinsi)
    {
        return redirect()->back();
    }

    
    public function edit(provinsi $provinsi)
    {
        return view('provinsi.edit',compact('provinsi'));
    }

    
    public function update(Request $request, provinsi $provinsi)
    {
        $request->validate([
            'nama_prov' => 'required'
        ]);
        $provinsi->nama_prov = $request->nama_prov;
        if($provinsi->save()){
            return redirect()->route('provinsi.index');
        }else{
            return redirect()->back();
        }
    }

    
    public function destroy(provinsi $provinsi)
    {
        if($provinsi->delete()){
            return redirect()->route('provinsi.index');
        }else{
            return redirect()->back();
        }
    }
}
