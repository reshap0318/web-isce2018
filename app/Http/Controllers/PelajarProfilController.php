<?php

namespace App\Http\Controllers;

use App\pelajar_profil;
use App\user;
use App\sekolah;
use Sentinel;
use Illuminate\Http\Request;

class PelajarProfilController extends Controller
{
    
    public function index()
    {
        $pelajar_profils = pelajar_profil::all();
        return view('pelajar_profil.index',compact('pelajar_profils'));
    }

    
    public function create()
    {
        $user = user::pluck('nama','id');
        $sekolah = sekolah::pluck('nama','id');
        return view('pelajar_profil.create',compact('user','sekolah'));
    }

   
    public function store(Request $request)
    {
        $request->validate([
            'nisn' => 'required|unique:pelajar_profils',
            'sekolah_id' => 'required',
        ]);

        $pelajar_profil = new pelajar_profil;
        $pelajar_profil->id = Sentinel::getuser()->id;
        $pelajar_profil->nisn = $request->nisn;
        $pelajar_profil->sekolah_id = $request->sekolah_id;
        //dd($pelajar_profil);
        if($pelajar_profil->save()){
            return redirect()->route('pelajar-profil.index');
        }else{
            return redirect()->back();
        }
    }

    
    public function show(pelajar_profil $pelajar_profil)
    {
        return view('pelajar_profil.show',compact('pelajar_profil'));
    }

    
    public function edit(pelajar_profil $pelajar_profil)
    {
        //dd($pelajar_profil);
        $user = user::pluck('nama','id');
        $sekolah = sekolah::pluck('nama','id');
        return view('pelajar_profil.edit',compact('user','sekolah','pelajar_profil'));
    }

  
    public function update(Request $request, pelajar_profil $pelajar_profil)
    {
        $request->validate([
            'nisn' => 'required',
            'sekolah_id' => 'required',
        ]);
        $pelajar_profil->id = Sentinel::getuser()->id;
        $pelajar_profil->nisn = $request->nisn;
        $pelajar_profil->sekolah_id = $request->sekolah_id;
        if($pelajar_profil->update()){
            return redirect()->route('pelajar-profil.index');
        }else{
            return redirect()->back();
        }
    }

    
    public function destroy(pelajar_profil $pelajar_profil)
    {
        if($pelajar_profil->delete()){
            return redirect()->route('pelajar-profil.index');
        }else{
            return redirect()->back();
        }
    }
}
