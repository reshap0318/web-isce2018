<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use App\kegiatan;
use App\user;
use Activation;
use App\pelajar_profil;
use App\mahasiswa_profil;
use App\roleuser;
use App\user_lomba as peserta;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;

class loginController extends Controller
{

    //protected $redirectTo = 'dashboard';

    public function username()
    {
        return 'username';
    }


    public function login()
    {
    	return view('login.login');
    }

    public function masuk(Request $request)
    {
        //dd($request->request);
    	$request->validate([
    		'username' => 'required',
    		'password' => 'required',
    	]);
        //dd(Sentinel::authenticate($request->all()));
        if ($user = Sentinel::authenticate($request->all())) {
            toast()->success('Berhasil Login');
            return redirect()->route('home.dashboard');
        }else{
            toast()->error('Gagal Login');
            return redirect()->back(); 
        }

    }


    public function daftar()
    {
        $kegiatan = kegiatan::pluck('nama','id');
    	return view('login.daftar',compact('kegiatan'));
    	//formseusai yang dibutuhkan
    }

    public function mendaftar(Request $request)
    {
    	$request->validate([
            'username'      => 'required|unique:users',
            'password'      => 'required|min:4:|same:confirm_password',
            'nama'          => 'required',
            'email'         => 'required|unique:users',
            'no_hp'         => 'required',
            'tempat_lahir'  => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'alamat'        => 'required',
            'lomba'         => 'required',
            'foto' => 'image|mimes:jpg,png,jpeg,gif',
        ]);
        $coba = ['username' => $request->username,'password'=>$request->password];
        //dd([$request->request,$coba, $request->all()]);
        $user = new user;
        $user->username = $request->username;
        $user->password = bcrypt($request->password);
        $user->nama = $request->nama;
        $user->email = $request->email;
        $user->no_hp = $request->no_hp;
        $user->tempat_lahir = $request->tempat_lahir;
        $user->tanggal_lahir = $request->tanggal_lahir;
        $user->jenis_kelamin = $request->jenis_kelamin;
        $user->alamat = $request->alamat;
        $path = 'img/foto'; 
        $oldfile = $user->foto;
        if ($request->hasFile('foto') && $request->foto->isValid()) {
            $fileext = $request->foto->extension();
            
            $filename = $request->file('foto')->getClientOriginalName();
            //dd([$fileext,$filename]);
            //Real File
            $filepath = $request->file('foto')->storeAs($path, $filename, 'local');
            //foto File
            $realpath = storage_path('app/'.$filepath);
            $user->foto = $filename;
            //hapus foto lama
            File::delete(storage_path('app'.'/'. $path . '/' . $oldfile));
            File::delete(public_path($path . '/' . $oldfile));
        }

        //dd($user);

        if($user->save()){ 
            $activation = Activation::create($user);
            $activation = Activation::complete($user, $activation->code);
            
                $role = new roleuser;
            if($request->lomba==1){
                $mahasiswa = new mahasiswa_profil;
                $mahasiswa->id = $user->id;
                $mahasiswa->save();

                $role->user_id = $user->id;
                $role->role_id = '2';
                $role->save();
            }elseif($request->lomba==2){
                $pelajar = new pelajar_profil;
                $pelajar->id = $user->id;
                $pelajar->save(); 

                $peserta = new peserta;
                $peserta->user_id = $user->id;
                $peserta->kegiatan_id = '2';
                $peserta->save();

                $role->user_id = $user->id;
                $role->role_id = '3';
                $role->save();
            }


            toast()->success('Berhasil Membuat Akun');
            if ($user = Sentinel::authenticate($coba)) {
                
                return redirect()->route('home.dashboard');
            }
            return redirect('/');
        }else{
            toast()->error('Gagal Membuat Akun');
            return redirect()->back();
        }
    }

    public function keluar()
    {
        Sentinel::logout();
        return redirect('/');
    } 
}
