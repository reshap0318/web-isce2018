<?php

namespace App\Http\Controllers;

use App\user_lomba;
use App\user;
use App\kegiatan;
use App\kelompok;
use Illuminate\Http\Request;

class UserLombaController extends Controller
{
    public function index()
    {
        $user_lombas = user_lomba::all();
        return view('user_lomba.index',compact('user_lombas'));
    }

    public function create()
    {
        $user = user::pluck('nama','id');
        $kegiatan = kegiatan::pluck('nama','id');
        $kelompok = kelompok::pluck('nama','id');
        return view('user_lomba.create',compact('user','kegiatan','kelompok'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'kegiatan_id' => 'required',
            'kelompok_id' => 'required',
        ]);
        $user_lomba = new user_lomba;
        $user_lomba->user_id = $request->user_id;
        $user_lomba->kegiatan_id = $request->kegiatan_id;
        $user_lomba->kelompok_id = $request->kelompok_id;
        if($user_lomba->save()){
            return redirect()->route('user-lomba.index');
        }else{
            return redirect()->back();
        }
    }

    public function show(user_lomba $user_lomba)
    {
        dd('belum');
    }

    public function edit(user_lomba $user_lomba)
    {
        $user = user::pluck('nama','id');
        $kegiatan = kegiatan::pluck('nama','id');
        $kelompok = kelompok::pluck('nama','id');
        return view('user_lomba.edit',compact('user','kegiatan','kelompok','user_lomba'));
    }


    public function update(Request $request, user_lomba $user_lomba)
    {
        $request->validate([
            'user_id' => 'required',
            'kegiatan_id' => 'required',
            'kelompok_id' => 'required',
        ]);
        $user_lomba->user_id = $request->user_id;
        $user_lomba->kegiatan_id = $request->kegiatan_id;
        $user_lomba->kelompok_id = $request->kelompok_id;
        if($user_lomba->update()){
            return redirect()->route('user-lomba.index');
        }else{
            return redirect()->back();
        }
    }

    public function destroy(user_lomba $user_lomba)
    {
        if($user_lomba->delete()){
            return redirect()->route('user-lomba.index');
        }else{
            return redirect()->back();
        }
    }
}
