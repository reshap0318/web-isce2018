<?php

use Illuminate\Database\Seeder;

class kegiatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kegiatans')->insert([
			    [		
			    		'id' 			=> '1',
			    		'nama'			=> 'Hackathon',	
			    		'tema' 			=> 'Sosial dan Pendidikan',
			    		'waktu_mulai' 	=> '2018-09-07',
						'waktu_berakhir'=> '2018-09-14',
						'insert'		=> '150.000,00',
						'lokasi'		=> 'Universitas ANDALAS',
						'latar_belakang'=> 'Saat sekarang ini banyak cara yang dapat digunakan untuk mengatasi berbagai masalah yang datang, salah satunya IT atau Informaon Technology dengan perangkat lunak yang ada membantu diberbagai masalah. IT saat ini dapat ditemukan diberbagai plaorm, yakni web, desktop, maupun mobile. Banyak perangkat lunak dengan berbagai fungsinya tercipta untuk membantu kehidupan. Melihat fenomena yang berkembang, maka pada rangkaian kegiatan ISCE 2018 ini menjadi salah satu wadah dalam mengembangkan inovasi baru, cerdas, dan dapat bermanfaat untuk memecahkan masalah yang ada.',
						'tujuan'		=> 'Adapun tujuan dari kegiatan ini untuk mengasah ide, mengembangkan inovasi baru, dan menciptakan perangkat lunak yang dapat digunakan dalam menyelesaikan permasalahan terkait masalah sosial dan pendidikan.',
						'deskripsi'		=> 'Hackathon dengan cakupan peserta Nasional akan berakhir dengan presentasi 5 tim yang lolos ke babak final pada 9 November 2018. Kegiatan ini dilaksanakan untuk melihat seberapa besar kemampuan dalam ngoding, disamping itu juga diadakan Expo dan Bazar untuk menampilkan bakat dan memeriahkan kegiatan ISCE 2018.',	
			    ],[		
			    		'id' 			=> '2',
			    		'nama'			=> 'Olimpiade Komputer',	
			    		'tema' 			=> 'Bersama IT is Funny',
			    		'waktu_mulai' 	=> '2018-09-03',
						'waktu_berakhir'=> '2018-09-28',
						'insert'		=> '50.000,00',
						'lokasi'		=> 'Universitas ANDALAS',
						'latar_belakang'=> 'Perkembangan zaman akan selalu diiku dengan perkembangan teknologi. Generasi yang unggul sangat dibutuhkan dalam bidang ilmu pengetahuan dan teknologi guna membantu Indonesia sebagai negara yang sedang berkembang. Para siswa dan siswi memiliki peranan penng dan berpotensi dalam mewujudkan cita-cita bangsa. Sejalan dengan hal tersebut, kegiatan-kegiatan ilmiah yang terarah selayaknya mendapatkan perhaan khusus sebagai wadah kreavitas dan pengembangan SDM.  
Sejalan dengan hal tersebut, Sistem Informasi Universitas Andalas melalui kegiatan ISCE 2018 turut berperan akf melalui kegiatan Olimpiade Komputer Tingkat SMA se-Sumatra Barat dalam upaya mengembangkan ilmu komputer khususnya di kalangan siswa SMA. Peningkatan kualitas sumber daya manusia terutama generasi muda yang menguasai ilmu pengetahuan khususnya di bidang komputer sangat diperlukan guna mengantarkan Indonesia sebagai negara yang inovaf di bidang  teknologi komputer.',
						'tujuan'		=> 'Adapun tujuan dari kegiatan ini adalah untuk mengetahui kemampuan mengenai berbagai hal tentang komputer, meningkatkan minat tentang komputer, meningkatkan semangat kompesi, serta menyaring kemampuan dibidang komputer siswa-siswi se-Sumatra Barat.',
						'deskripsi'		=> 'Kegiatan Olimpiade Komputer untuk siswa/i ngkat SMA se-Sumatra Barat yang diadakan dengan dua tahap yakni tahap penyisihan pada 5 November 2018 dan tahap ﬁnal pada 6 November 2018, disamping itu juga diadakan Expo dan Bazar untuk menampilkan bakat dan memeriahkan kegiatan ISCE 2018',	
			    ],
		 ]);
    }
}
