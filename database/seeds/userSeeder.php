<?php

use Illuminate\Database\Seeder;

class userSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('provinsis')->insert([
			    [		
			    		'id' 			=> '1',
			    		'nama_prov'		=> 'SUMATERA BARAT',			    		
			    ]
		 ]);

    	DB::table('kab_kotas')->insert([
			    [		
			    		'id' 			=> '1',
			    		'nama_kab_kota'	=> 'Kota Padang',	
			    		'prov_id'		=> '1',			    		
			    ]
		 ]);

    	DB::table('kecamatans')->insert([
			    [		
			    		'id' 			=> '1',
			    		'nama_kec'		=> 'Nanggalo',
			    		'kab_kota_id'	=> '1',				    		
			    ]
		 ]);

        DB::table('users')->insert([
			    [		
			    		'id' 			=> '1',
			    		'username'		=> 'aldogitu',
			    		'nama'			=> 'Reinaldo Shandev Pratama',	
			    		'email' 		=> 'reshap03@gmail.com',
			    		'password' 		=> bcrypt('admin'),
			    		'no_hp' 		=> '085805506719',
			    		'tempat_lahir' 	=> 'padang',
			    		'tanggal_lahir' => '2018-08-26',
			    		'jenis_kelamin' => 'L',
			    		'kecamatan_id'	=> '1',	
			    		'foto'			=> 'Alogo.png',
			    		'alamat'		=> 'jalan rambun bulan no 17 berok gunung panggilun padang',	
			    		'permissions' 	=> '{"home.dashboard":true}',
			    ],
		 ]);
         DB::table('roles')->insert([
			    [		
			    		'id'=>'1',
			    		'slug' 			=> 'admin',
			    		'name' 			=> 'Admin',
			    		'permissions' 	=> '{"home.dashboard":true,"pemilik-akun.index":true,"pemilik-akun.create":true,"pemilik-akun.store":true,"pemilik-akun.show":true,"pemilik-akun.edit":true,"pemilik-akun.update":true,"pemilik-akun.destroy":true,"role-akun.index":true,"role-akun.create":true,"role-akun.store":true,"role-akun.show":true,"role-akun.edit":true,"role-akun.update":true,"role-akun.destroy":true,"role-akun.permissions":true,"role-akun.simpan":true,"provinsi.index":true,"provinsi.create":true,"provinsi.store":true,"provinsi.show":true,"provinsi.edit":true,"provinsi.update":true,"provinsi.destroy":true,"kabupaten-kota.index":true,"kabupaten-kota.create":true,"kabupaten-kota.store":true,"kabupaten-kota.show":true,"kabupaten-kota.edit":true,"kabupaten-kota.update":true,"kabupaten-kota.destroy":true,"kecamatan.index":true,"kecamatan.create":true,"kecamatan.store":true,"kecamatan.show":true,"kecamatan.edit":true,"kecamatan.update":true,"kecamatan.destroy":true,"sekolah.index":true,"sekolah.create":true,"sekolah.store":true,"sekolah.show":true,"sekolah.edit":true,"sekolah.update":true,"sekolah.destroy":true,"universitas.index":true,"universitas.create":true,"universitas.store":true,"universitas.show":true,"universitas.edit":true,"universitas.update":true,"universitas.destroy":true,"pelajar-profil.index":true,"pelajar-profil.create":true,"pelajar-profil.store":true,"pelajar-profil.show":true,"pelajar-profil.edit":true,"pelajar-profil.update":true,"pelajar-profil.destroy":true,"mahasiswa.index":true,"mahasiswa.create":true,"mahasiswa.store":true,"mahasiswa.show":true,"mahasiswa.edit":true,"mahasiswa.update":true,"mahasiswa.destroy":true,"kelompok.index":true,"kelompok.create":true,"kelompok.store":true,"kelompok.show":true,"kelompok.edit":true,"kelompok.update":true,"kelompok.destroy":true,"kegiatan.index":true,"kegiatan.create":true,"kegiatan.store":true,"kegiatan.show":true,"kegiatan.edit":true,"kegiatan.update":true,"kegiatan.destroy":true,"user-lomba.index":true,"user-lomba.create":true,"user-lomba.store":true,"user-lomba.show":true,"user-lomba.edit":true,"user-lomba.update":true,"user-lomba.destroy":true,"berkas.index":true,"hackathon.index":true,"hackathon.ktm":true,"hackathon.grup":true,"hackathon.addanggota":true,"hackathon.proposal":true}',
			    ],[		
			    		'id'=>'2',
			    		'slug' 			=> 'hackathon',
			    		'name' 			=> 'Hackathon',
			    		'permissions' 	=> '{"password.request":true,"password.email":true,"password.reset":true,"home.dashboard":true,"universitas.create":true,"universitas.store":true,"universitas.edit":true,"universitas.update":true,"mahasiswa.show":true,"mahasiswa.edit":true,"mahasiswa.update":true,"kelompok.create":true,"kelompok.store":true,"kelompok.edit":true,"kelompok.update":true,"kelompok.destroy":true,"user-lomba.create":true,"user-lomba.store":true,"user-lomba.edit":true,"user-lomba.update":true,"user-lomba.destroy":true,"berkas.index":true,"hackathon.index":true,"hackathon.ktm":true,"hackathon.grup":true,"hackathon.addanggota":true,"hackathon.proposal":true,"hackathon.dellproposal":true}',
			    ],[		
			    		'id'=>'3',
			    		'slug' 			=> 'olkom',
			    		'name' 			=> 'Olimpiade Komputer',
			    		'permissions' 	=> '{"password.request":true,"password.email":true,"password.reset":true,"home.dashboard":true,"pemilik-akun.show":true,"provinsi.index":true,"provinsi.create":true,"provinsi.store":true,"provinsi.show":true,"provinsi.edit":true,"provinsi.update":true,"provinsi.destroy":true,"kabupaten-kota.index":true,"kabupaten-kota.create":true,"kabupaten-kota.store":true,"kabupaten-kota.show":true,"kabupaten-kota.edit":true,"kabupaten-kota.update":true,"kabupaten-kota.destroy":true,"kecamatan.index":true,"kecamatan.create":true,"kecamatan.store":true,"kecamatan.show":true,"kecamatan.edit":true,"kecamatan.update":true,"kecamatan.destroy":true,"sekolah.create":true,"sekolah.store":true,"sekolah.show":true,"sekolah.edit":true,"sekolah.update":true,"pelajar-profil.index":true,"pelajar-profil.create":true,"pelajar-profil.store":true,"pelajar-profil.show":true,"pelajar-profil.edit":true,"pelajar-profil.update":true,"pelajar-profil.destroy":true,"berkas.index":true}',
			    ],
			    
		 ]);
		 DB::table('role_users')->insert([
			    [		
			    		'user_id' 		=> '1',
			    		'role_id' 			=> '1',
			    ]
		 ]);
		 DB::table('activations')->insert([
			    [		
			    		'user_id' 		=> '1',
			    		'code' 			=> '1S4u7lJzehk62xDm3DgYgXXYWtbHE6gSP',
			    		'completed' 	=> '1',
			    ]
		 ]);
    }
}
