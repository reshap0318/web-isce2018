<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSekolahsTable extends Migration
{
    public function up()
    {
        Schema::create('sekolahs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('kontak')->nullable();
            $table->string('nama_kontak')->nullable();
            $table->string('kode_pos')->nullable();
            $table->integer('kecamatan_id')->unsigned()->nullable();
            $table->text('alamat')->nullable();
            $table->timestamps();

            $table->foreign('kecamatan_id')->references('id')->on('kecamatans')->onDelete('cascade')->onUpdate('cascade');
        });
    }
    public function down()
    {
        Schema::dropIfExists('sekolahs');
    }
}
