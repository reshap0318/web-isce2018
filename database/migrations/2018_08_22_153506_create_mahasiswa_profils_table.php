<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMahasiswaProfilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswa_profils', function (Blueprint $table) {
            $table->integer('id')->unsigned()->primary();
            $table->string('nim')->nullable()->unique();
            $table->integer('universitas_id')->unsigned()->nullable();
            $table->string('jurusan')->nullable();
            $table->string('ktm')->nullable();

            $table->timestamps();

            $table->foreign('id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('universitas_id')->references('id')->on('universitas')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswa_profils');
    }
}
